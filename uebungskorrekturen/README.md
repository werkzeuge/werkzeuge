# Digitale Abgabe-kontrolle:

## Funktionen:
modus|Beschreibung
---|---
nameingruppe | Prüft ob einer der Namensteile im Ordnernamen auch ein Namensteil des Dateinamen ist; ggf. Präfix "Problem_" zufügen
zuruecksetzen | Entfernt das Präfix "Problem_" bei allen Ordnern
uebernehmen | Kopiert neueste (d.h. geänderte) Datei in alle Ordner derselben Abgabegruppe (ausgenommen Probleme)
gruppenzahl | Gibt die Anzahl der Abgabegruppen an
punktedatei | Legt eine Datei im Format "Gruppe,Punkte" an
punkteeintragen | Überträgt Punkte von GruppePunkte.csv in Bewertungen[...].csv

## Vorbedingungen:
- auf dem Gerät läuft Texlua
- Abgaben sind im PDF-Format
- Abgaben haben die Benennung "A042-Musterfrau-Beispiel.pdf" für Aufgabe 42 von Lisa Musterfrau und Mark Beispiel (als Bestätigung, dass die angegebenen mitgearbeitet haben; muss ggf. noch in der Datei überprüft werden)
- Download der Dateien, Bewertungstabelle und upload des Feedbacks in Moodle aktiviert ("Korrektureinstellungen" in Moodle)

## Ablaufs-vorschlag:
1. Lade "alle Abgabedateien als zip" und die Bewertungstabelle über die Moodle-Abgabe herunter
2. 'texlua abgleich.lua nameingruppe'
3. Abgleichen, dass die mit "Problem_..." bezeichneten Ordner in Ordnung sind (ggf. Ordner + Namen aus Bewertungstabelle löschen)
4. 'texlua abgleich.lua zuruecksetzen'
5. 'texlua abgleich.lua punktedatei'
6. jeweils eine Abgabe je Gruppe kontrollieren und Punkte in "GruppePunkte.csv" eintragen
7. 'texlua abgleich.lua punkteeintragen'
8. 'texlua abgleich.lua uebernehmen'
9. Abgaben zipen
10. Bewertungstabelle und zip in Moodle hochladen

## Umgang mit Nachreichungen

1. Aufgabe in Moodle auf "nicht sichtbar" schalten und letzte Abgabemöglichkeit nach hinten stellen
2. Ein Mitglied der Gruppe suchen und neben der Abgaben bearbeiten -> "Abgabe bearbeiten" und dann die Datei hochladen
3. Letzte Abgabemöglichkeit zurückstellen und Aufgabe wieder sichtbar schalten
