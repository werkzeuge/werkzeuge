#!/usr/bin/lua
if not lfs then
    lfs = require("lfs")
end
ignore = {
    A=true,
    AG=true,
    assignsubmission=true,
    file=true,
    VJA=true,
    VNEIN=true,
    pdf=true,
}

--TODO: wirklich erst nach lower? vorher wäre besser
--anpassen, falls es Probleme gibt
--TODO: hinzufuegen, wenn weitere hinzukommen
--Tipp: uconv -x any-nfd <<<ä gibt D-normalisiertes ä aus
--(v.a. für Mäc-User)
ersatz = {
    ["ä"] = "ae",
    ["ä"] = "ae",
    ["ö"] = "oe",
    ["ö"] = "oe",
    ["ü"] = "ue",
    ["ü"] = "ue",
    ["ß"] = "ss",
    ["á"] = "a",
    ["á"] = "a",
    ["å"] = "a",
    ["å"] = "a",
    ["ç"] = "c",
    ["ç"] = "c",
    ["é"] = "e",
    ["é"] = "e",
    ["ğ"] = "g",
    ["ğ"] = "g",
    ["ı"] = "i",
    ["ł"] = "l",
    ["ó"] = "o",
    ["ó"] = "o",
    ["ş"] = "s",
    ["ş"] = "s",
}
ersetze = function(s)
    for o,e in pairs(ersatz) do
        s = s:gsub(o,e)
    end
    return s
end

zweimalgross = function(s)
    return s:match("[A-Z][A-Z]")
end


dateiin = function(dir)
    local datei
    for d in lfs.dir(dir) do
        if d ~= "." and d ~= ".." then
            datei = d
        end
    end
    return datei
end

copy = function(s,t)
    local sfile = io.open(s, "r")
    local str = sfile:read("*a")
    sfile:close()

    local tfile = io.open(t, "w")
    tfile:write(str)
    tfile:close()
    lfs.touch (t,lfs.attributes(s,"modification"))
end

werkzeuge = {}
hilfe = {}

werkzeuge.nameingruppe = function()
    for abg in lfs.dir(".") do
        if lfs.attributes(abg,"mode") ~= "directory" or
                abg == "." or abg == ".." or
                abg:sub(1,8) == "Problem_"
        then
            goto continue
        end
            
        local datei = dateiin(abg)

        local gruppe = {}
        --wir tolerieren CamelCase-Namen aber auch
        --NAMENSSCHREIER -> Großbuchstabe als Namensstart
        --nur, wenn keine 2 Großbuchstaben hintereinander
        local camel = (zweimalgross(datei) and "") or "A-Z"
        for dnamensteil in string.gmatch(datei,
                "[^0-9%s.,_-][^"..camel.."0-9%s.,_-]*") do
            if not ignore[dnamensteil] then
                dnamensteil = ersetze(dnamensteil:lower())
                gruppe[dnamensteil] = true
--                     print(dnamensteil)
            end
        end
        local ingruppe = false
        for namensteil in string.gmatch(abg, "[^0-9%s-_][^A-Z0-9%s_-]+") do
            if not ignore[namensteil] then
                namensteil = ersetze(namensteil:lower())
                ingruppe = gruppe[namensteil] or ingruppe
--                     print(namensteil)
            end
        end
        if not ingruppe then
            os.rename(abg,"Problem_"..abg)
            print(abg,datei)
        end
        ::continue::
    end
end
hilfe.nameingruppe = "Prüft ob einer der Namensteile im Ordnernamen"..
                    " auch ein Namensteil des Dateinamen ist."

werkzeuge.zuruecksetzen = function()
    for abg in lfs.dir(".") do
        if abg:sub(1,8) == "Problem_" then
            print(abg:sub(9))
            os.rename(abg,abg:sub(9))
        end
    end
end
hilfe.zuruecksetzen = "Entfernt das Präfix Problem_ bei allen Ordnern."

werkzeuge.uebernehmen = function()
    local neueste = {}
    for abg in lfs.dir(".") do
        if lfs.attributes(abg,"mode") ~= "directory" or
                abg == "." or abg == ".." or
                abg:sub(1,8) == "Problem_"
        then
            goto continue
        end
        
        local ag = abg:sub(1,5)
        local datei = dateiin(abg)
        local zeit = lfs.attributes(abg.."/"..datei,"modification")
        neueste[ag] = neueste[ag] or {o=abg, z=zeit, d=datei}
        if neueste[ag].z < zeit then
            neueste[ag] = {o=abg, z=zeit, d=datei}
        end
        ::continue::
    end
    for abg in lfs.dir(".") do
        if lfs.attributes(abg,"mode") ~= "directory" or
                abg == "." or abg == ".." or
                abg:sub(1,8) == "Problem_"
        then
            goto continue
        end
        
        local ag = abg:sub(1,5)
        local datei = dateiin(abg)
        local zeit = lfs.attributes(abg.."/"..datei,"modification")
        if neueste[ag].z > zeit then
            copy((neueste[ag].o).."/"..(neueste[ag].d),
                 abg.."/"..datei)
            print("kopiere",(neueste[ag].o).."/"..(neueste[ag].d),
                 abg.."/"..datei)
        end
        ::continue::
    end
end
hilfe.uebernehmen = "Kopiert neueste (d.h. geänderte) Datei"..
                    " in alle Ordner derselben Abgabegruppe "..
                    "(ausgenommen Probleme)"

werkzeuge.gruppenzahl = function()
    local anz = 0
    local bekannt = {}
    for abg in lfs.dir(".") do
        if lfs.attributes(abg,"mode") ~= "directory" or
                abg == "." or abg == ".." or
                abg:sub(1,8) == "Problem_"
        then
            goto continue
        end
        
        local ag = abg:sub(1,5)
        if not bekannt[ag] then
            bekannt[ag] = true
            anz = anz + 1
        end
        
        ::continue::
    end

    print("Anzahl der Abgaben ist " .. anz);
end
hilfe.gruppenzahl = "Gibt die Anzahl der Abgabegruppen an."

werkzeuge.punktedatei = function()
    local pfile = io.open("GruppePunkte.csv", "r")
    if pfile then
        print("Schon vorhanden, wird nicht überschrieben!")
        pfile:close()
        return
    end

    local bekannt,gruppen = {},{}
    for abg in lfs.dir(".") do
        if (lfs.attributes(abg,"mode") ~= "directory" and
         abg:sub(-3,-1):lower() ~= "pdf") or
                abg == "." or abg == ".." or
                abg:sub(1,8) == "Problem_"
        then
            print ("ignored ", abg)
            goto continue
        end
        
        local ag = abg:sub(1,5)
        if not bekannt[ag] then
            bekannt[ag] = true
            table.insert(gruppen,ag)
        end
        
        ::continue::
    end
    table.sort(gruppen)
    
    pfile = io.open("GruppePunkte.csv", "w")
    for _,ag in ipairs(gruppen) do
        pfile:write(ag..",\n")
    end
    pfile:close()
end
hilfe.punktedatei = "Legt eine Datei im Format Gruppe,Punkte an."

werkzeuge.punkteeintragen = function()
    local pfile = io.open("GruppePunkte.csv", "r")
    if not pfile then return end
    
    local punkte = {}
    
    for pag in pfile:lines() do
        local ag=pag:sub(1,5)
        punkte[ag] = pag
    end
    pfile:close()
    
    local mname = nil
    for fname in lfs.dir(".") do
        if fname:sub(1,11) == "Bewertungen" then
            mname = fname
        end
    end
    if not mname then return end
    
    mfile = io.open(mname,"r")
    mtmpfile = io.open(mname.."-tmp", "w")
    
    for pzeile in mfile:lines() do
        local ag = pzeile:match("AG[0-9][0-9][0-9]")
        if ag and punkte[ag] then
            pzeile = pzeile:gsub(ag..",,",punkte[ag]..",")
        end
        mtmpfile:write(pzeile.."\n")
    end
    mfile:close()
    mtmpfile:close()
    os.rename(mname.."-tmp",mname)
end
hilfe.punkteeintragen = "Überträgt Punkte von GruppePunkte.csv in "..
                    "Bewertungen[...].csv"
                    
print("Arbeitsverzeichnis:",lfs.currentdir())
if werkzeuge[arg[1]] then
    werkzeuge[arg[1]]()
else
    print("Aufruf: texlua abgleich.lua WERKZEUG")
    print("Werkzeuge:")
    for k,_ in pairs(werkzeuge) do
        print(k,hilfe[k])
    end
end
