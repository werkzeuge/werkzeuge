ag=1
temp=0

for f in *; do
  mv "$f" "${f// /_}";
done

for line in *; do
  temp=$(grep -oP 'AG\K.*?(?=-)' <<< $line)
  echo temp=$temp
  echo $ag
  if [ "$temp" == "$ag" ]
     then
	 echo $line
	 rm -r $line
     else
	 ag=${temp}
  fi
done
