# Plagiatschecker
Bei Programmierabgaben wird teilweise derselbe Code von mehreren Gruppen abgegeben. Wenn dies ausdrücklich ungewünscht ist und verfolgt werden soll, hilft ggf. dieses Programm.
Quelle: https://github.com/jplag/jplag

## Ablauf:
1. Alle Abgaben via Moodle (zip) herunterladen
2. Bei Gruppenabgaben nur eine Datei im (Unter-)Ordner haben (bspw. durch das Skript im Anhang)
3. 'java -jar jplag-2.12.1-SNAPSHOT-jar-with-dependencies.jar -r [pathToOutput] -s -m [NoOfResults | percentage"%"] -l java19 [PathToFiles]'
    Im Ordner ist eine, ggf. Veraltete Version des Skriptes. Eine aktuelle findet man hier https://github.com/jplag/jplag. "-l" Parameter ist nach der Sprache anzupassen.
4. Im results-folder "index.html" öffnen und nachkontrollieren, ob die Ähnlichkeit für Plagiatsvorwurf reicht.