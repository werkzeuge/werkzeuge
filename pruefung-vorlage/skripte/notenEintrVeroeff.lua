local ods = require("loadodsfile")
local csv=require("csv")
local CSVTab,csvread,csvwrite = csv.CSVTab,csv.csvread,csv.csvwrite
local odsTableValuesToCSVTab=csv.odsTableValuesToCSVTab
local tonumberorna,NA = csv.tonumberorna,csv.NA
dofile("options.lua")
prnr=prnr or(pr[2] and pr[2].ordner and pr[2].ordner ~= "" and 2) or 1
pr=prconf[prnr]

local colToTab = function(sp,cnr)
  local col,ret = ods.tableValues(sp,cnr,nil,cnr),{}
  for i,t in ipairs(col) do
    ret[i] = t[1].value
  end
  return ret
end

local resPath = pr.path..'/'..pr.results
print(resPath)
local sp = ods.getTable(
  ods.load(resPath):loadContent())
local gradeCol
for i=1,100 do
  if ods.tableValues(sp,i,1,i,1)[1][1] and
      ods.tableValues(sp,i,1,i,1)[1][1].value == "Note" then
    gradeCol = i
    break
  end
end
if not gradeCol then
  print("Notenspalte nicht gefunden, Abbruch.")
  return
end
local spval = ods.tableValues(sp,1,nil,gradeCol,nil)
local resCSV = odsTableValuesToCSVTab(spval)
resCSV=resCSV:selectrows(function(r) return r.ID ~= "" end)

local cols = {"ID","Note"}

if pr.uploadPoints then
  table.insert(cols,"Summe")
  local lastEx = tonumber(resCSV:colname(gradeCol-2):sub(2))
  for i=1,lastEx do
    table.insert(cols,"A"..i)
  end
end

--nur ID+Note:
local idList = resCSV:colsub(cols)
--wer nicht da war, weiß das und kennt die eigene ID nicht:
idList=idList:selectrows(
  function(r) return tostring(r.Note):match("[0-9],?[0-9]?") end
)
idList:transformCol("Note",
  function(v)
    v = tostring(v)
    if #v < 3 then
      v = v..",0"
    end
    return v
  end
)
idList=idList:sorted("ID",false,sortReplace)

csvwrite(idList,resPath:sub(1,-5)..".txt","\t",'')

if not pr.agnesexp then
  return
end
--ab hier wollen wir eine Liste zum Hochladen erstellen:

assert(resCSV["Matrikel-Nr."],
  "Mnr.-Spalte heißt nicht 'Matrikel-Nr.'")
resCSV:addCol("AgnesNote",{})
for i=1,resCSV:nrows() do
  local s = tostring(resCSV["Note"][i]):gsub(",",".")
  if tonumber(s) then
    --math.floor(2.3 * 100) ist 229 in Lua-> +0.1
    s = tostring(math.floor(100*tonumber(s)+0.1))
  end
  resCSV["AgnesNote"][i] = s
  resCSV["Matrikel-Nr."][i] = tostring(resCSV["Matrikel-Nr."][i])
end


local resmnr = resCSV:colAsIndex("Matrikel-Nr.")

local agnesPath = pr.path..'/'..pr.agnesexp
local agnes = csvread(agnesPath.."-export",";",nil,
  --[[nonumconv=]]true)
  assert(agnes,"Datei ".. agnesPath.."-export nicht vorhanden!"..
    " Bitte nach Download umbenennen, "..
    "da der Upload den Originalnamen braucht." )

for i=1,agnes:nrows() do
  local mn = agnes.mtknr[i]
  local s = resCSV.AgnesNote[resmnr[mn]]
  agnes.bewertung[i] = s
end

agnes = agnes:selectrows(function(r) 
    return tonumber(r.bewertung) or r.bewertung == "NE"
  end)

csvwrite(agnes,agnesPath,";",'')
