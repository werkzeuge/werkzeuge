local ods
if kpse then
  kpse.set_program_name("lualatex")
  ods=require("odsfile")
  --im CTAN ist leider eine Version von odsfile mit veraltetem
  --Modulmechanismus, texlua (obwohl >=5.3) unterstützt den aber noch
  --(im Gegensatz zu normalem Lua >= 5.2)
  if not ods or ods == true and odsfile then
    ods=odsfile
  end
else
  local myof = io.open("mytexoptions.lua", "rb")
  if myof then
    myof:close()
  else
    print("Lua wird nicht als texlua ausgeführt, bitte Pfad zu texlive in mytexoptions.lua angeben."..
        " Bitte auch luarocks install luazip für das Paket zip ausführen.")
    myof = io.open("mytexoptions.lua","w")
    myof:write('texlivebase="/usr/local/texlive/2019"')
    myof:close()
  end
  dofile("mytexoptions.lua")
  package.path=package.path..";"..texlivebase..
      "/texmf-dist/tex/lualatex/odsfile/?.lua;"..
      texlivebase.."/texmf-dist/tex/luatex/luaxml/?.lua"
  module = function() end
  local loadsav = load
  local oldglob = {}
  for k,v in pairs(_G) do
    oldglob[k]=v
  end
  ods=require("odsfile")
  --im CTAN ist leider eine Version von odsfile mit veraltetem
  --Modulmechanismus
  if ods == true then
    ods = {}
    for k,v in pairs(_G) do
      if not rawequal(v,oldglob[k]) then
        ods[k]= v
      end
    end
    zip = ods.zip
    ods.zip = nil
    load = oldglob.load
  end
end

return ods
