# Skripte und Dateien in EThI

## Dateien aus der Übung des Semesters:
  * `Agnes.ods`: enthält 3 Tabellen
    * Hier kann nun einfach die Datei aus der Platzvergabe
    exportiert (und ggf. konvertiert) werden. Weitere Bearbeitung ist
    nicht mehr notwendig. Ausnahme: Tabellen aus ÜWP
    und "normaler" Übung zusammenfassen.
    Zukünftig ist zu prüfen, ob diese Datei überhaupt noch nötig ist
    und nicht allein der Moodle-Export genügt.
    * ~~Tabelle 1: Teilnehmende (mind.: Mnr,Nachn.,Vorn.)
    wird aus den Übungs-(oder VL-)Anmeldungen gewonnen, die sich
    direkt aus Agnes herunterladen lassen~~
    * restliche Tabellen nicht mehr nötig
  * `Agnes.csv`: erste Tabelle der `Agnes.ods` exportiert nach
  CSV. Mit kleiner Änderung von `MoodleAgnesZuListe.lua` könnte
  auf diesen Schritt verzichtet werden.
  * `MoodleKomplett.csv` wird am Ende des Semesters aus Moodle exportiert.
  Der Merge mit Agnes (wenn überhaupt) erfolgt nun per Matrikelnr.
  Die MoodleKomplett.csv bitte
  Trenner **Semikolon** aus Moodle exportieren.
  * `StudierendeGesamt.csv` wird vom Skript `MoodleAgnesZuListe.lua`
  erzeugt. Je nach Ablauf (s.u.: mit Prüferfunktion oder ohne)
  können hier die an einer Klausur teilnehmenden Studierenden
  manuell markiert werden, alte Übungsscheine nachgetragen
  und Daten ergänzt werden. Die Skripte überschreiben idR
  keine partiell in dieser Datei vorhandenen Daten, s. Ablauf.
  Wenn möglich sollte die Datei aber gar nicht manuell editiert
  werden und die weiteren Eingabemöglichkeiten der Skripte genutzt
  werden.

## Dateien im Prüfungsordner (z.B. YYYY-MM-DD-pr)
Wir lassen die inhaltlichen Dateien weg, also solche die mit den
Klausuraufgaben zu tun haben.

  * `modul-ergebnisse-YYYY-MM-DD.ods`
  (aus `klausur-ergebnisse-vorlage.ods`): wird manuell aus der Übernahme
  der Teilnehmenden in die Datei der letzten Pr und Anpassung der
  Aufgaben etc. erstellt. Dient als Skripteingabe für den Upload-
  generator für die ID-Punkte-Noten-Datei sowie bei Nutzung der
  Prüferfunktion.
  * `klausur-deckblaetter-JJJJ-MM-TT`: Deckblätter und Namens-
  sowie Platzlisten. Nutzt die per Skript generierte Datei
  `teilnehmende.csv`.
  * `teilnehmende.csv`: Enthält Mnr,Namen,IDs und Sitzplätze sowie
  ggf. Bemerkungen. Auch für leere Deckblätter gibt es Zeilen.
  * `PRX_MNrID.csv`: Enthält nur die Kombination Mnr<->ID und wird
  von den Skripten angelegt, um eine versehentliche Vertauschung der
  IDs zu verhindern, wenn z.B. nur die Plätze geändert werden. Bei
  Änderung der Teilnehmenden (z.B. weitere Zugelassene) muss die Datei
  gelöscht werden.
  * `sitzplan.ods`: Sitzplan mit verwendeten Sitzen aus denen dann
  ein Skript die Sitzzuteilung generiert. Kaputte Tische müssen jedes
  Jahr **neu überprüft** werden.
  * `Namenkorrektur.csv`: hiermit können einzelne Namen ergänzt oder
  modifiziert werden, ohne die Datei `StudierendeGesamt.csv` manuell
  zu editieren. Dies trifft insbesondere auf Studierende mit alten
  Übungsscheinen zu, die nicht in Moodle oder Agnes angemeldet sind.
  Da die Prüferfunktion keine sauberen Namen exportiert (sortname
  enthält nur einen komplett klein geschriebenen und ggf. gekürzten
  Namen), ist dies nötig.
  * `prmanuell.csv`: Mnr.-Liste mit Studierenden, die nicht über die
  Prüferfunktion angemeldet sind, d.h. alle, wenn die Funktion gar
  nicht benutzt wird. Dies kann auch bei Aktivierter Prüferfunktion
  genutzt werden, wenn eine bessere Datenquelle (PDF) für die Namen
  gewünscht ist.
  Da die Namen für fast alle eh verzeichnet sind,
  muss keine (echte) Namenspalte enthalten sein und es kann
  stattdessen die Datei `Namenkorrektur.csv` genutzt werden.
  `prmanuell.csv` lässt sich ohne
  Prüferfunktion am einfachsten generieren, wenn
  man die `StudierendeGesamt.csv` alphabetisch sortiert, ein Flag
  für jeden angemeldeten Studierenden setzt, dann danach sortiert und
  die Mnr.-Spalte nach `prmanuell.csv` kopiert.
  * `prf_XXXX_YYYYY_AA_ZZ.csv`: Daten aus der Prüferfunktion.
  Auf keinen Fall manuell bearbeiten, da sonst der Import der
  später vom Skript generierten Datei scheitern kann. Der Name
  ist in die `options.lua` (s.u.) einzutragen. Diese Datei sollte
  umbenannt werden (`-export`), da die Importdatei wieder genau diesen
  Namen haben muss.
  * `prf_XXXX_YYYYY_AA_ZZ.csv-export`: vom Skript
  `notenEintrVeroeff.lua` benötigte Datei zum Upload über die
  Prüferfunktion. Hinweis: Der Upload speichert die Daten noch nicht,
  sondern gibt sie nur in die Maske auf der Webseite ein. Speichern
  und dann späteres Freigeben also nicht vergessen.

## externe Dateien

  * `ue.csv` o.ä. (Option `uesAltPath`): alte Übungsscheine mit Jahr.
  Aus Datenschutzgründen
  soll diese Datei nicht in den Semesterordner kopiert werden bzw.
  in ein git mit Zugriff für alle SHKs des Semesters eingecheckt
  werden.

## Skripte und Optionen:
Alle Skripte funktionieren mit Lua ab 5.2, insbesondere auch mit
dem Befehl texlua von texlive ab 2019 (ggf. auch älter, nicht
getestet). Einige Skripte benötigen odsfile.lua aus dem CTAN-Paket
`odsfile` (s.u.), alle anderen Abhängigkeiten sind im Ordner
enthalten, sofern texlua genutzt wird. Ansonsten müssen `luazip` und
`lpeg` per luarocks installiert werden. Diese sind Teil der Binärdatei
texlua. Die Skripte werden ohne Optionen nur mit
`lua <Skriptname>.lua` aufgerufen, d.h. wenn man Lua-Dateien z.B.
mit texlua öffnen lässt, genügt je nach Desktop auch ein Doppelklick.
Einige Skripts geben aber auch etwas aus, z.B., dass die
Sitzplatzaufteilung nicht funktioniert.

  * `options.lua`: Im Idealfall die einzig zu editierende Datei
  im Sriptordner. Die Optionen sind erläutert.
  * `MoodleAgnesZuListe.lua`: Zusammenführen der Moodle- und
  Agnes-Daten, eintragen alter Übungsscheine und der Klausuranmeldungen.
    * Verwendet: `Agnes.csv`,`MoodleKomplett.csv`,`prmanuell.csv`,
    `prf_XXXX_YYYYY_AA_ZZ.csv`,`Namenkorrektur.csv`,`ue.csv`,
    `StudierendeGesamt.csv`
    * generiert: `StudierendeGesamt.csv`
    * lädt: `oo.lua`,`csv.lua`
  * `listeZuPr.lua`: Generieren der Listen für die Prüfung.
    * Verwendet: `StudierendeGesamt.csv`,`sitzplan.ods`,`PRX_MNrID.csv`
    * generiert: `PRX_MNrID.csv`,`teilnehmende.csv`
    * lädt: `oo.lua`,`csv.lua`,`loadodsfile.lua`
  * `notenEintrVeroeff.lua`: Generieren der Import-Liste für die
  Prüfendenfunktion und einer ID-Notenliste für die Webseite.
    * Verwendet: `ergebnisse-YYYY-MM-DD.ods`,`prf_XXXX_YYYYY_AA_ZZ.csv`
    * generiert: `ergebnisse-YYYY-MM-DD.txt`,`prf_XXXX_YYYYY_AA_ZZ_import.csv`
    * lädt: `oo.lua`,`csv.lua`,`loadodsfile.lua`
  * `mytexoptions.lua`: nötig für `odsfile.lua`, wenn nicht texlua
  verwendet wird, um den Pfad der texlive-Installation zu finden.
  * `loadodsfile.lua`: Wrapper-Skript, um `odsfile.lua` mit lua und
  texlua zu laden.
  * `csv.lua`: Verarbeiten von CSV-Dateien und ähnlich strukturierten
  Daten (Eigenentwicklung, bei Änderungen bitte Header anpassen).
  * `oo.lua`: Klassen und einige weitere nützliche Funktionen zur
  Behandlung von Tabellen. Achtung: ändert den globalen Namespace.

## Ablauf:

  * Semesterbeginn:
    * `Agnes.ods` anlegen (Export aus der Platzvergabe)
    ~~und nach Ende der Zulassungsfristen die
    Listen aus den Mails ergänzen~~
  * Semesterende:
    * Export der Punkte aus Moodle in `MoodleKomplett.csv`
    * ggf. fehlden Studierende in Agnes.csv ergänzen (nachtr.
    Anm. u.ä.)
    * `MoodleAgnesZuListe.lua` starten, um alte ÜS
    einzutragen (wenn Zugriff) oder ggf. manuell
    in `StudierendeGesamt.csv` eintragen.
  * Prüfung mit Prüfendenfunktion:
    * Prüfungsdaten exportieren zu `prf_XXXX_YYYYY_AA_ZZ.csv`,
    und dann die Datei mit Suffix `-export` umbenennen
    und deren Namen in `options.lua` eintragen +
    ggf. manuelle Daten zusätzlich anlegen.
    Für die Generierung von Listen und Deckblättern ist es ggf.
    ratsam, eine PDF aus Agnes zu exportieren und dann mit
    pdftotext u.ä. eine CSV manuell zu generieren, da in der
    heruntergeladenen CSV die Namen verkürzt sein können.
    * `options.lua` aktualiseieren und `MoodleAgnesZuListe.lua` erneut
    starten: Studierende mit Anmeldungen und ohne Zulassung werden
    ausgegeben (stdout). Diese ggf. anschreiben oder Daten korrigieren.
    * `sitzplan.ods` für die Teilnehmerzahl anpassen, passende
    Sortier- und Trennreihenfolge wählen (z.B. Raum nach Nachname,
    im Raum nach Mnr. rückwärts). Ziel ist jeweils die genauen
    Sitznachbarn so unvorhersehbar wie praktisch möglich zu machen.
    * `listeZuPr.lua` laufen lassen und damit die Listen für
    Deckblätter etc. erzeugen. So bald die Teilnehmer fix feststehen,
    die Datei `PRX_MNrID.csv` einchecken, damit alle WiMis/SHKs
    sicher dieselben IDs verwenden.
    * erst wenn die Plätze und IDs fix sind, die Daten aus
    `teilnehmende.csv` in `ergebnisse-YYYY-MM-DD.ods`
    übernehmen und Aufgabenzanhl etc. anpassen.
    * nach der Klausur Punkte etc. in die
    `ergebnisse-YYYY-MM-DD.ods` eintragen und Notengrenzen
    in der Datei festlegen.
    * Vor der Einsicht `notenEintrVeroeff.lua` erstmals starten,
    um die ID-Notenliste zu generieren.
    * Nach der Einsicht Einträge in `ergebnisse-YYYY-MM-DD.ods`
    korrigieren und `notenEintrVeroeff.lua` erneut laufen lassen,
    um `prf_XXXX_YYYYY_AA_ZZ_.csv` aus der Datei mit
    Suffix `-export` zu erzeugen und die Datei **ohne Suffix**
    hochladen.
    * Wurden bei/nach der Einsicht Änderungen beschlossen, die auch
    nicht anwesende Studierende betreffen (z.B. mildere Korrektur
    einer Teilaufg. o.ä.), dann auch `ergebnisse-YYYY-MM-DD.txt`
    erneut hochladen und Studierende über z.B. Moodle informieren.
  * Prüfung ohne Prüferfunktion: analog nur ohne alle Schritte mit
  `prf_XXXX_YYYYY_AA_ZZ.csv`
  * Nach der zweiten Prüfung:
    * aus der `StudierendeGesamt.csv` eine Liste der ÜS erstellen und
    an das Prüfungsamt geben (**TODO**: automatisierbar?).
    * Eintragen der ÜS in Agnes (TE-Status) bzw. Moodle (falls dafür
    verwendet)


# Arbeitsschritte

* `odsCsv.sh`:
Konvertiert mittels libreOffice eine aus der Platzvergabe gewonnene
ODS-Datei zu CSV für die Skripte.
* `MoodleAgnesZuListe.lua`:
Daten aus der Excel-Tabelle der Platzergabe und den Moodle-Punkten
mergen. Ziel ist hier die (dauerhafte) Speicherung der Übungsscheine
mit korrekten Daten. Zudem kann die Anmeldeliste einer Klausur
und eine Datei mit alten Übungsscheinen abgeglichen werden.
Komplett fehlende Personen sollten in die ods der Platzvergabe ergänzt werden.
Die erstellt die Datei StudierendeGesamt.csv aus der dann die Platzvergabe
der Prüfung generiert wird.
* `gesamtZuPr.lua`:
Erstellt mittels Sitzplan und StudierendeGesamt.csv eine Datei
teilnehmedePr1.csv im Prüfungsordner. Die Aufteilung wird mittels
`options.lua` angegeben.


