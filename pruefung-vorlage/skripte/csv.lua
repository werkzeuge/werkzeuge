--(C) 2020-22 Frank Fuhlbrück
--This file (csv.lua) can be distrubuted and modified without any 
--restrictions. If this file is distributed with other files
--this only holds for csv.lua, other parts may have more restrictive
--licences.
--Version: frafl-20230421
require("oo")
lpeg = require("lpeg")

--we don't want nil in our tables as this breaks the # operator
local NA = {}
setmetatable(NA,{
  __tostring = function() return "NA" end,
  __lt = function(x,y) return x == NA and y ~= NA end
})

local CSVTab
CSVTab = {
  new = function()
    return setmetatable({
        __colnames={},
        __colindices={},
        __sep=";",
        __strdel='"',
      },CSVTab)
  end,
  
  cloneHead = function(self)
    local ret = CSVTab()
    for j=1,#self do
      local name = self.__colnames[j]
      ret[j] = {}
      ret[name] = ret[j]
      ret.__colnames[j] = name
      ret.__colindices[name] = j
    end
    return ret
  end,
  
  colname = function(self,i)
      return self.__colnames[i]
  end,
  
  colrename = function(self,old,new)
    local j = (type(old)=="number" and old) or self.__colindices[old]
    if not j then error('Column "' .. old .. '" does not exist.') end
    self[new] = self[old]
    if old ~= j then
      self[old] = nil
    end
    self.__colnames[j] = new
    self.__colindices[new] = j
  end,
  
  colidx = function(self,i)
    return self.__colindices[i]
  end,
  
  --does not check for unique values!
  colAsIndex = function(self,idc)
    return kvswap(self[idc])
  end,
  
  addCol = function(self,name,newc)
    local j = #self+1
    self[j] = {}
    self[name] = self[j]
    self.__colnames[j] = name
    self.__colindices[name] = j
    if newc == NA or type(newc) ~= "table" then
      for i=1,#self[1] do
        self[j][i] = newc
      end
    else
      for i=1,#self[1] do
        self[j][i] = newc[i]
      end
    end
  end,
  
  transformCol = function(self,j,f,default,nonna)
    if type(f) == "table" then
      local t = f
      f = function(v) return t[v] or default end
    end
    local col = self[j]
    local last = default
    for i,v in ipairs(col) do
      if not nonna or v ~= NA then
        v = f(v,last,i)
        col[i] = v 
        last = v
      end
    end
  end,
  
  sorted = function(self,by,dec,sortReplace)
    --sortReplace sollte Tablle oder Funktion sein
    sortReplace = sortReplace or {}
    if type(sortReplace) == "table" then
      local sr = sortReplace
      sortReplace = function(i)
        return sr[i]
      end
    end
    --by sollte eine Funktion sein, die auf Zeilen arbeitet oder
    --ein Spaltenindex oder Name
    if type(by) ~= "function" then
      local byKey = by
      by = function(ri,rj)
        return (sortReplace(ri[byKey]) or ri[byKey] or NA) < 
          (sortReplace(rj[byKey]) or rj[byKey] or NA)
      end
    end
    --wir formen by weiter um in eine Funktion, die auf Zeilenindices
    --arbeitet:
    local ropts = {indices=true,names=true}
    local byij
    if not dec then
      byij = function(i,j)
        return by(self:row(i,ropts),self:row(j,ropts))
      end
    else
      byij = function(i,j)
        return by(self:row(j,ropts),self:row(i,ropts))
      end
    end
    
    indCol = {} for i=1,self:nrows() do indCol[i]=i end
    table.sort(indCol,byij)
    local ret = self:cloneHead()
    for j,col in ipairs(ret) do
      local oldcol = self[j]
      for i=1,self:nrows() do
        col[i] = oldcol[indCol[i]]
      end
    end
    return ret
  end,
  
  transformColNonNa = function(self,j,f,default)
    self:transformCol(j,f,default,true)
  end,
  
  nrows = function(self)
    return (self[1] and #self[1]) or 0
  end,
  
  row = function(self,i,opts)
    opts = opts or {indices=true,names=false}
    local r = {}
    for j=1,#self do
      if opts.indices then
        r[j] = self[j][i]
      end
      if opts.names then
        r[self.__colnames[j]] = self[j][i]
      end
    end
    return r
  end,
  
  rows = function(self,opts)
    opts = opts or {indices=true,names=true}
    local nrows = self:nrows()
    local currow = 0
    return function()
      currow = currow + 1
      if currow <= nrows then
        return self:row(currow,opts)
      end
    end
  end,
  
  addRow = function(self,r,opts)
    if not self[1] then return end
    local i = #self[1]+1
    opts = opts or {indices=true,names=false}
    for j=1,#self do
      if opts.indices then
        self[j][i] = (r[j] ~= false) and (r[j] or NA)
      end
      if opts.names then
        if r[self.__colnames[j]] or
            r[self.__colnames[j]] == false then
          self[j][i] = r[self.__colnames[j]]
        end
      end
    end
    return i
  end,
  
  findFirst = function(self,crit)
    if not self[1] then return nil end
    for i=1,#self[1] do
      if crit(self:row(i,{names=true,indices=true}),i) then
        return i
      end
    end
    return nil
  end,
  
  selectrows = function(self,crit)
    local ret = self:cloneHead()
    if not self[1] then return ret end
    local newi=0
    for i=1,#self[1] do
      if crit(self:row(i,{names=true,indices=true}),i) then
        newi=newi+1
        for j=1,#self do
          ret[j][newi] = self[j][i]
        end
      end
    end
    return ret
  end,
  
  --does *not* copy by default:
  colsub = function(self,which,copy)
    local ret = CSVTab()
    for i,s in ipairs(which) do
      local name
      if type(s) == "string" then
        name = s
      else
        name = self.__colnames[s]
      end
      assert(self[s],"Column '"..s.."' does not exist!")
      ret[i] = (copy and dcopy(self[s])) or self[s]
      ret[name] = ret[i]
      ret.__colnames[i] = name
      ret.__colindices[name] = i
    end
    return ret
  end,
  
  mergedWith = function(self,with,by,opts)
    if not self[by][1] then error() end
    opts = opts or {
      suffix="-2",
      ignore={[""]=true},
      appendignored = true,
    }
    local suffix,ignore = opts.suffix,opts.ignore
    local m = self:cloneHead()
    local j=#m
    local withcolidx = {}
    local byidx = {self.__colindices[by],
          with.__colindices[by]}
    for jwith=1,#with do
      if jwith ~= byidx[2] then
        j=j+1
        withcolidx[j]=jwith
        local name = with.__colnames[jwith]
        if m.__colindices[name] then
          name = name .. suffix
        end
        m.__colnames[j] = name
        m.__colindices[name] = j
        m[j] = {}
        m[name] = m[j]
      end
    end
    local withbyidx = with:colAsIndex(by)
    local selfby=self[by]
    local i=0
    for iself=1,#selfby do
      local byv = selfby[iself]
      if byv and not ignore[byv] then
        local wbi = withbyidx[byv]
        --this is useful to find superfluous entries in table with: 
        withbyidx[byv] = nil
        i=i+1
        for j=1,#self do
          m[j][i] = self[j][iself]
        end
        if wbi then
          for j=#self+1,#m do
            m[j][i] = with[withcolidx[j]][wbi]
          end
        else
          for j=#self+1,#m do
            m[j][i] = NA
          end
        end
      end
    end
    for byv,wbi in pairs(withbyidx) do
      if not ignore[byv] then
        i=i+1
        for j=1,#self do
          m[j][i] = NA
        end
        for j=#self+1,#m do
          m[j][i] = with[withcolidx[j]][wbi]
        end
        m[byidx[1]][i] = byv
      end
    end
    if not appendignored then
      return m
    end
    --ignored keys are useless for merges but we may want to keep
    --"half" records for them
    for iself=1,#selfby do
      local byv = selfby[iself]
      if not byv or ignore[byv] then
        i=i+1
        for j=1,#self do
          m[j][i] = self[j][iself]
        end
        for j=#self+1,#m do
          m[j][i] = NA
        end
      end
    end
    local withby = with[by]
    for iwith=1,#withby do
      local byv = withby[iwith]
      if not byv or ignore[byv] then
        i=i+1
        m[byidx[1]][i] = byv
        for j=#self+1,#m do
          m[j][i] = with[withcolidx[j]][iwith]
        end
        for j=1,#self do
          m[j][i] = NA
        end
      end
    end
    return m
  end,
  
  --no checks are performed, format must match
  concat = function(self,snd)
    local ret = self:cloneHead()
    for j=1,#self do
      ret[j] = gluetables(self[j],snd[j])
      ret[ret.__colnames[j]] = ret[j]
    end
    return ret
  end,
  
  __index = function(self,i)
    if type(i) == "number" then
      return rawget(self,i)
    end
    if type(i) == "string" then
      return rawget(self,i) or CSVTab[i]
    end
    return rawget(self,i[2])[i[1]]
  end,
  
  __newindex = function(self,i,v)
    if type(i) == "number" then
      return rawset(self,i,v)
    end
    if type(i) == "string" then
      return rawset(self,i,v)
    end
    rawget(self,i[2])[i[1]]=v
  end,
  
  tostring = function(self,sep,strdel,minlen)
    minlen = minlen or 0
    tost = function(x) return 
      (type(x)=="string" and strdel..x..strdel) or
      ({[true]="TRUE",[false]="FALSE"})[x] or
      (x==nil and "NA") or tostring(x)
    end
      
    fill = function(x) return (#x>=minlen and x) or fill(x..' ') end
    sep = sep or self.__sep
    strdel = strdel or self.__strdel 
    if not self[1] then return "" end
    local ret = fill(tost(self.__colnames[1]))
    for j=2,#self do 
      ret = ret .. sep .. fill(tost(self.__colnames[j]))
    end
    for i=1,#self[1] do
      ret = ret .. "\n"..fill(tost(self[1][i]))
      for j=2,#self do
        ret = ret .. sep .. fill(tost(self[j][i]))
      end
    end
    return ret
  end,
}
CSVTab.__tostring = CSVTab.tostring
CSVTab.__call = CSVTab.__index
makeclass(CSVTab)

local csvread = function(file,sep,strdel,nonumconv)
  local f = io.open(file, "rb")
  if f then f:close() else return nil end
  sep = sep or ';'
  strdel = strdel or '"'
  local field = strdel * lpeg.Cs(((lpeg.P(1) - strdel) + 
                  lpeg.P(strdel..strdel) / strdel)^0) * strdel +
                  lpeg.C((1 - lpeg.S(sep..'\n'..strdel))^0)
  local record = lpeg.Ct(field * (sep * field)^0) * (lpeg.P'\n' + -1)
  local ret = CSVTab()
  local i = -1
  for line in io.lines(file) do
    i=i+1
    if i == 0 then
      local head = lpeg.match(record,line)
      assert(head)
      for j,v in ipairs(head) do
        ret.__colnames[j] = v
        ret.__colindices[v] = j
        ret[j] = {}
        ret[v] = ret[j]
      end
    else
      local lres = lpeg.match(record,line)
      local nc = not nonumconv
      assert(lres,"Wrong line format! File:'"..file.."'" ..
        "' Separator:"..sep  .." String Delimiter:"..strdel)
      for j,v in ipairs(lres) do
        ret[j][i] = (nc and tonumber(v)) or (v == "TRUE" and true) or
          (v ~= "FALSE" and v ~= "NA" and v) or
          (v == "NA" and NA) or false
      end
    end
  end
  return ret
end

local csvwrite = function(csv,file,sep,strdel)
  local f = io.open(file,"w")
  if f then 
    f:write(csv:tostring(sep,strdel))
    f:close()
    return true
  else
    return false
  end
end

local tonumberorna = function(x)
  return tonumber(x) or NA
end

--takes the structure returned by
--odsfile.tableValues(odsfile.getTable(odsfile.load(x):loadContent())
--from CTAN package "odsfile"
--for some ods file name x and turns it into a CSVTab
--first line *must* be header
local odsTableValuesToCSVTab = function(values)
  local ret = CSVTab()
  for _,namef in ipairs(values[1]) do
    ret:addCol(namef.value,{})
  end
  for i=2,#values do
    for j,valf in ipairs(values[i]) do
      local v = valf.value
      ret[j][i-1] = tonumber(v) or (v == "TRUE" and true) or
          (v ~= "FALSE" and v ~= "NA" and v) or
          (v == "NA" and NA) or false
    end
  end
  return ret
end




return {
  csvread=csvread,
  CSVTab=CSVTab,csvwrite=csvwrite,
  tonumberorna=tonumberorna,
  odsTableValuesToCSVTab=odsTableValuesToCSVTab,
  NA=NA
}
