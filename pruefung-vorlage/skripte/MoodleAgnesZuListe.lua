--Modul csv organisiert 2dim als Tabellen Array von Spaltenarrays
--d.h. z.B. agnes[j][i] greift auf i.Zeile,j.Spalte zu
--agnes{i,j} und agnes[{i,j}] greifen (mit mehr Aufwand) auf
--dasselbe Element zu, zum Setzen muss agnes[{i,j}]=...
--genutzt werden, s.u. (oder agnes[j][i]=...).
local csv=require("csv")

local CSVTab,csvread,csvwrite = csv.CSVTab,csv.csvread,csv.csvwrite
local tonumberorna,NA = csv.tonumberorna,csv.NA
dofile("options.lua")

--Werte, die keinen Informationsgehalt haben (fehlen oder nur Space)
local noinfo = function(v)
  return ({[NA]=true,[""]=true,[" "]=true})[v]
end


--1.Phase Daten aus Moodle und Agnes zusammenführen
--dadurch sind die korrekten Schreibweisen der Namen aus Agnes vorhanden
--Vorname ggf. nicht mehr
stud=csvread(studierendeGesPath,CSVsep,CSVstrdelRead)
if not stud then
  agnes=csvread(agnesLehrePath,CSVsep,CSVstrdelRead)
  agnes=agnes:colsub({"Matrikelnummer","CMS","Nachname","Vorname"})
  agnes:colrename("Matrikelnummer","MatrikelNr")
  agnes:transformColNonNa("MatrikelNr",tostring)
  agnes:addCol("Agnes",true)
  moodle=csvread(moodleGesPath,CSVsep,CSVstrdelRead)
  moodle:colrename(moodleGesamtstatus,"UESaktuell")
  moodle:colrename(moodleSchriftlich,"schriftlich")
  moodle:colrename(moodleMndlMC,"online")
  if not moodleMNR then
    error("Bitte in den Optionen (moodleMNR) angeben, welche Spalte in Moodle die Matrikelnr. enthält.")
  end
  moodle:colrename(moodleMNR,"MatrikelNr")
  moodle:transformColNonNa("MatrikelNr",tostring)
  moodle:colrename("Nachname","NachnameMoodle")
  moodle:colrename("Vorname","VornameMoodle")
  moodle=moodle:colsub({"MatrikelNr","UESaktuell","schriftlich","online","NachnameMoodle","VornameMoodle"})

  moodle=moodle:selectrows(function(r) return r.CMS ~= "" end)
  moodle:transformCol("UESaktuell",{[moodleUESVal]=true},false)
  moodle:addCol("Moodle",true)
  stud=agnes:mergedWith(moodle,moodleAgnesMergeBy)
  for i=1,stud:nrows() do
    if stud{i,"Nachname"} == NA and stud{i,"NachnameMoodle"} ~= NA then
      stud[{i,"Nachname"}] = stud{i,"NachnameMoodle"}
      stud[{i,"Vorname"}] = stud{i,"VornameMoodle"}
    end
    --seit 22/23 hat Agnes keine Nachnamen mehr
    if noinfo(stud{i,"Vorname"}) and stud{i,"VornameMoodle"} ~= NA then
      stud[{i,"Vorname"}] = stud{i,"VornameMoodle"}
    end
  end
  stud=stud:colsub({"MatrikelNr","CMS","Nachname","Vorname","Agnes","Moodle","UESaktuell","schriftlich","online"})
  --entstandene NAs werden durch false ersetzt:
  stud:transformCol("Agnes",{[true]=true},false)
  stud:transformCol("Moodle",{[true]=true},false)
  csvwrite(stud,studierendeGesPath,CSVsep,CSVstrdelWrite)
else
  stud:transformColNonNa("MatrikelNr",tostring)
end
--Ende 1.Phase


--2.Phase automatische und/oder manuelle Prüfungsanmeldungen
--in separaten Listen abgleichen
addPr = function(nr)
  pr=prconf[nr]
  sortname="sortname"..nr
  if not pr.path then
    return
  end
  local pran = csvread(pr.path..'/'..(pr.agnes or "-"),
    CSVsep,CSVstrdelRead)
  local prman = csvread(pr.path..'/'..(pr.manuell or "-"),
    CSVsep,CSVstrdelRead)
  if not pran and not prman then
    return
  end
  if pran then
    pran = pran:colsub({"sortname","mtknr"})
  end
  if prman then
    if not prman["sortname"] then
      prman:addCol("sortname","")
    end
    prman = prman:colsub({"sortname","mtknr"})
  end
  if not pran --[[and prman]] then
    pran = prman
  elseif pran and prman then
    pran = pran:concat(prman)
  end
  pran:colrename("mtknr","MatrikelNr")
  pran:colrename("sortname",sortname)
  pran:transformColNonNa("MatrikelNr",tostring)
  pran:addCol("PR"..nr.."angemeldet",true)
  stud=stud:mergedWith(pran,"MatrikelNr")
  --entstandene NAs werden durch false ersetzt:
  stud:transformCol("UESaktuell",{[true]=true},false)
  stud:transformCol("PR"..nr.."angemeldet",{[true]=true},false)

  if pr.namenkor then
    local namenkor = csvread(pr.path..'/'..(pr.namenkor or "-"),
      CSVsep,CSVstrdelRead)
    namenkor:transformColNonNa("MatrikelNr",tostring)

    local studmnr = stud:colAsIndex("MatrikelNr")
    for i=1,namenkor:nrows() do
      studi=studmnr[namenkor[{i,"MatrikelNr"}]]
--       stud[{studi,"Nachname"}] = namenkor{i,"Nachname"}
--       stud[{studi,"Vorname"}] = namenkor{i,"Vorname"}
    end
  end

  --Diese Funktion filtert nicht nur, sie modifiziert auch direkt.
  local useSortnameAsName = function(r,i)
    local sname = r[sortname] or NA
    if (not pr.prefersortname and
                  (not noinfo(r.Nachname) and not noinfo(r.Vorname)))
                  or noinfo(sname) then
      return false
    end
    local cpos = sname:find(',')
    stud.Nachname[i] = sname:sub(1,cpos-1)
    stud.Vorname[i] = sname:sub(cpos+2)--+2 wg. Space nach Komma
    return true
  end
  local snameused = stud:selectrows(useSortnameAsName)

  if not pr.prefersortname then
    print("Für folgende Studierende wird mangels besserer Daten sortname genutzt:\n")
    print(snameused:tostring("\t","",8))
    print("\nBei falscher Namensschreibung (z.B. Umlaute), bitte Namenskorrektur nutzen.\n\n")
  end
end

--nur für die 1.Pr:
if not stud["PR1angemeldet"] then
  addPr(1)
  stud:transformCol("UESaktuell",{[true]=true},false)
  csvwrite(stud,studierendeGesPath,CSVsep,CSVstrdelWrite)
end
--für 1. schon vorhanden, nun für die zweite:
if not stud["PR2angemeldet"] then
  addPr(2)
  if stud["PR1angemeldet"] then
    stud:transformCol("PR1angemeldet",{[true]=true},false)
  end
  stud:transformCol("UESaktuell",{[true]=true},false)
  csvwrite(stud,studierendeGesPath,CSVsep,CSVstrdelWrite)
end
--Ende 2.Phase
--Stattdessen kann auch manuell die Datei unter studierendeGesPath
--editiert werden und alle angemeldeten auf TRUE gesetzt bzw.
--komplett fehlende Einträge ergänzt werden

--3.Phase abgleichen alter ÜS
if not stud.UESalt then
  stud:addCol("UESalt",NA)
  uesalt = csvread("../../../uebersicht/ue.csv")
  uesalt:colrename("Mnr.","MatrikelNr")
  uesalt:transformColNonNa("MatrikelNr",tostring)
  local dupl,studmnr = {},stud:colAsIndex("MatrikelNr")
  local copyFirstUES = function(r)
    local mnr = r.MatrikelNr
    if mnr == "" or not r.UES or dupl[mnr] then
      return false
    end
    dupl[mnr] = true--ein ÜS reicht
    if studmnr[mnr] then
      stud[{studmnr[mnr],"UESalt"}] = r.Jahr
    end
    return false--wir legen keine Zeilen an, kopieren direkt
  end
  uesalt:selectrows(copyFirstUES)
  csvwrite(stud,studierendeGesPath,CSVsep,CSVstrdelWrite)
end

local prKeinUES = function(nr) return function(r)
  return not r.UESaktuell and r.UESalt==NA and
      r["PR"..nr.."angemeldet"]
end end


if stud["PR1angemeldet"] then
  print("PR1 Angemeldet ohne Zulassung (bisher):")
  print(stud:selectrows(prKeinUES(1)):tostring("\t","",8))
end
if stud["PR2angemeldet"] then
  print("PR2 Angemeldet ohne Zulassung (bisher):")
  print(stud:selectrows(prKeinUES(2)):tostring("\t","",8))
end
--Ende 3.Phase (Rest siehe ListeZuPr.lua)



