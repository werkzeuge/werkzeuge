--(C) 2017-2019 Frank Fuhlbrück
--This file (oo.lua) can be distrubuted and modified without any 
--restrictions. If this file is distributed with other files
--this only holds for oo.lua, other parts may have more restrictive
--licences.
--Version: frafl-20190510

makeclass = function(c,inh)
    local idx = nil
    if inh then
        for _,pc in ipairs(inh) do
            for k,v in pairs(pc) do
                if not c[k] then
                    rawset(c,k,v)
                end
            end
        end
        idx = function(o,i)
            local r = rawget(o,i)
            if r then
                return r
            end
            for _,pc in ipairs(inh) do
                r = pc[i]
                if r then
                    return r
                end
            end
        end
    end
    setmetatable(c, {
        __call = function (cls, ...)
            return cls.new(...)
        end,
        __index = idx
    })
    if c.__index == nil then
        if c.__index_chain then
            c.__index = function(o,i)
                local r = c.__index_chain(o,i)
                if not r then
                    return c[i]
                end
            end
        else
            c.__index = c
        end
    end

    if c.__concat == nil then
        c.__concat = concatWithToString
    end
end

concatWithToString = function(t1,t2)
    return  tostring(t1) ..  tostring(t2)
end

--copying the metatable in one or two steps
--returns target thus "return cpmt(f,t)"  is possible
cpmetatable = function(f,t)
    t = t or {}
    setmetatable(t,getmetatable(f))
    return t
end
cpmt = cpmetatable
savemt = function(f)
    local mt = getmetatable(f)
    return function(t)
        setmetatable(t,mt)
        return t
    end
end

--copy tables until depth l (only copies values, not keys!)
dcopy = function(t,l)
    l = l or 1
    if type(t) == 'table' and l > 0 then
        local ret = {}
        cpmt(t,ret)
        for k,v in pairs(t) do
            ret[k] =  dcopy(v,l-1)
        end
        return ret
    else
        return t
    end
end

--mmipairs shall respect metamethods for all LUA versions,
--while rawipairs shall ignore them
rawipairs=ipairs
mmipairs=ipairs
if(_VERSION=="Lua 5.2") then
    local ipit = function(t,i)
        i = i+1
        local v = t[i]
        if v then
            return i,v
        end
    end
    mmipairs = function (t)
        return ipit,t,0
    end
else
    local ipit = function(t,i)
        i = i+1
        local v = rawget(t,i)
        if v then
            return i,v
        end
    end
    rawipairs = function (t)
        return ipit,t,0
    end
end

ipairsuntil = function(t,ut)
    local ipit = mmipairs(t)
    return function(t,i)
        local j,v = ipit(t,i)
        if v and v ~= ut then
            return j,v
        end
    end,t,0
end

--printable table representation until depth l
dump = function(t,l,pre,post,sep)
    pre = pre or "{"
    post = post or "}"
    sep = sep or ", "
    l = l or 1
    if type(t) == 'table' and l > 0 then
        local s,lastk,lastv = pre
        for k,v in pairs(t) do
            if lastv ~= nil then 
                s = s .. dump(lastk,l-1)..":".. dump(lastv,l-1) .. sep
            end
            lastv,lastk = v,k
        end
        return s .. dump(lastk,l-1)..":".. dump(lastv,l-1) .. post
    else
        return tostring(t)
    end
end

--printable table representation until depth l (only values)
dumpNoKeys = function(t,l,pre,post,sep)
    pre = pre or "{"
    post = post or "}"
    sep = sep or ", "
    l = l or 1
    if type(t) == 'table' and l > 0 then
        local s = pre
        local lastv
        for k,v in pairs(t) do
            if lastv ~= nil then 
                s = s  .. dumpNoKeys(lastv,l-1) .. sep
            end
            lastv = v
        end
        return s .. dumpNoKeys(lastv,l-1) .. post
    else
        return tostring(t)
    end
end

--printable table repr. until depth l (only values of int keys)
idump = function(t,l,pre,post,sep)
    pre = pre or "{"
    post = post or "}"
    sep = sep or ", "
    l = l or 1
    if type(t) == 'table' and l > 0 then
        local s = pre
        for k=1,#t do
             local v=t[k]
             s = s  .. idump(v,l-1) .. (k<#t and sep or "")
        end
        return s .. post
    else
        return tostring(t)
    end
end

--create statefull iterator from stateless one
statefull = function(sl,t)
    local it, s, k = sl(t)
    return function()
        local vals = {it(s,k)}
        k = vals[1]
        return table.unpack(vals)
    end
end

--checks if an iterator delivers a number n of values
--where n is in [rmin,rmax]
rangemany = function(it,rmin,rmax)
    local i = 0
    while it() and i <= rmax+1 do
        i=i+1
    end
    return i >= rmin and i <= rmax
end

tablerange = function(t,f,l)
    local ret = {}
    for i=f,l do
        ret[#ret+1] = t[i]
    end
    return ret
end

gluetables = function(t1,t2)
    local ret = {}
    local nt1=#t1
    for i=1,#t1+#t2 do
        ret[i] = (i <= nt1 and t1[i]) or t2[i-nt1]
    end
    return ret
end

settablerange = function(t,f,r)
    f=f-1
    for i=1,#r do
        t[i+f] = r[i]
    end
end

rev = function(t)
    local ret,j = {},#t
    for i=1,#t do
        ret[j] = t[i]
        j = j-1
    end
    return ret
end

--table of keys, s.t. keys(t)[k]=k
keys   = function(t)
    local ret = {}
    for k,v in pairs(t) do
        ret[k]=k
    end
    return ret
end

--table of keys, s.t. keys(t)[i]=k, i in 1,num. of keys
ikeys   = function(t)
    local ret = {}
    for k,v in pairs(t) do
        ret[#ret+1]=k
    end
    return ret
end

--table of keys, s.t. keys(t)[k]=k
kvswap   = function(t)
    local ret = {}
    for k,v in pairs(t) do
        ret[v]=k
    end
    return ret
end

--Tabelle der Schlüssel, numerisch geordnet, sortiert nach Funktion f
--f(k1,k2,t) erhält zu verlgleichende Schlüssel und die Tabelle
--gibt true zurück, falls k1 vor k2 stehen soll
ikeyssort = function(t,f)
    local ret = {}
    for k,v in pairs(t) do
        ret[#ret+1]=k
    end
    local fs = function(k1,k2)
        return f(k1,k2,t)
    end
    table.sort(ret,fs)
    return ret
end

numval = function(how)
    if how == "asc" then
        return function(k1,k2,t)
            return (t[k1] < t[k2]) or
                    (t[k1] == t[k2] and k1 < k2)
        end
    else
        return function(k1,k2,t)
            return (t[k1] > t[k2]) or
                    (t[k1] == t[k2] and k1 < k2)
        end
    end
end

--map übergibt auch den jew. Schlüssel, aber als 2. Argument
--Fkt. f mit nur einem Argument erhalten also nur den Wert
map = function(f,t)
    local ret = {}
    for k,v in pairs(t) do
        ret[k] = f(v,k)
    end
    return ret
end

mapi = function(f,it)
    local i = it()
    return function()
        return f(i())
    end
end

mapmod = function(f,t)
    for k,v in pairs(t) do
        t[k] = f(v,k)
    end
    return t
end

--auch reduce kann die Schlüssel nutzen
reduce = function(f,t,init,iter)
    iter = iter or pairs
    local ret = init
    for k,v in iter(t) do
        ret = f(ret,v,k)
    end
    return ret
end

prod = function(t,iter)
    return reduce(function(x,y) return x*y end,t,1)
end

sum = function(t,iter)
    return reduce(function(x,y) return x+y end,t,0)
end

whichmax = function(t,iter)
    return reduce(
        function(maxpair,v,k)
            return (maxpair.v < v and {v=v,k=k}) or maxpair
        end,t,{v=0,k=0},iter
    )
end

--Transponieren einer 2dim-Tabelle (d.h. Tab. von Tabellen)
--mit jeweils numerischen Schlüsseln
transp = function(t,inplace)
    local ret = {}
    for j=1,#(t[1]) do
        ret[j] = {}
        for i=1,#t do
            ret[j][i] = t[i][j] 
        end
    end
    return ret
end

blocks  = function(t,l)
    local bl = {}
    for i=1,#t do
        if i % l == 1 or l==1 then
            bl[#bl+1] = {}
        end
        bl[#bl][((i-1) % l)+1] = t[i]
    end
    return bl
end

splice  = function(t,l)
    local spl = {}
    for i=1,l do
        spl[i] = {}
    end
    for i=1,#t do
        local part = spl[((i-1) % l)+1]
        part[#part+1] = t[i]
    end
    return spl
end

desplice  = function(spl)
    local t,l,i = {},#spl,0
    local last = ""
    while last do
        i=i+1
        t[i] = spl[((i-1) % l)+1][((i-1)-((i-1)%l))/l+1]
        last = t[i]
    end
    return t
end

spliceStr  = function(s,l)
    local spl = {}
    for i=1,l do
        spl[i] = ""
    end
    for i=1,s:len() do
        local part = spl[((i-1) % l)+1]
        part = part .. s:sub(i,i)
        spl[((i-1) % l)+1] = part
    end
    return spl
end

despliceStr  = function(spl)
    local s,l,i = "",#spl,0
    local last = "-"
    while last ~= "" do
        i=i+1
        local j = ((i-1)-((i-1)%l))/l+1
        last = spl[((i-1) % l)+1]:sub(j,j)--gibt "" zurück,für j>len
        s = s .. last
    end
    return s
end

--recursive implementation of choose iterator (for reference)
chooseRec = function(t,n,tmax,tshift,nshift)
    tshift = tshift or 0
    nshift = nshift or 0
    tmax = tmax or #t
    local pos,nlit = tshift,nil
    if n ~= 1 then
        nlit = choose(t,n-1,tmax,tshift+1,nshift+1)
        pos=pos+1
        return function()
            local set = nlit()
            if (not set) and pos < tmax-(n-1) then
                pos = pos + 1
                nlit=choose(t,n-1,tmax,pos,nshift+1)
                set = nlit()
            elseif (not set) and pos >= tmax-(n-1) then
                return nil
            end
            set[nshift+1] = t[pos]
            return set
        end
    else
        return function()
            pos = pos + 1
            return ((pos <= tmax and {[nshift+1]=t[pos]}) or nil)
        end
    end
end

--k-element subsets of {1,...,n}
chooseInd = function(n,k)
    if k > n then
      return function() return nil end
    end
    if k == 0 then
      local done = false
      return function() 
        local ret=(not done and {}) or nil
        done=true 
        return ret 
      end
    end
    local ind = {}
    ind[1] = 0--always incrementable
    for i=2,k do
      ind[i] = n--never incrementable
    end
    return function()
      if not ind then
        return nil
      end
      local r = k
      while r >= 1 and ind[r] >= n-k+r do
        r = r - 1
      end
      if r == 0 then
        ind = nil
        return nil
      end
      ind[r] = ind[r]+1
      for i = r+1,k do
        ind[i] = ind[i-1]+1
      end
      return ind
    end
end

--k-element subtables of t
choose = function(t,k)
  local n = #t
  local cit = chooseInd(#t,k)
  return function()
    local ind=cit()
    if ind then
      local s = {}
      for i=1,k do
        s[i] = t[ind[i]]
      end
      return s 
    else
      return nil
    end
  end
end

perm = function(t,npos)
    local k = ikeys(t)
    local pos = 0
    npos = npos or 1
    if #k ~= 1 then
        local nlit = nil
        return function()
            local set = nlit and nlit()
            if (not set) and pos < #k then
                pos = pos + 1
                local tc = dcopy(t)
                tc[k[pos]] = nil
                nlit=perm(tc,npos+1)
                set = nlit()
            elseif (not set) and pos >= #k then
                return nil
            end
            set[npos] = t[k[pos]]
            return set
        end
    else
        local once = true
        return function()
            if once then
                once = false
                return {[npos]=t[k[1]]}
            else
                return nil
            end
        end
    end
end

kperm = function(t,k)
    local chooseit = choose(t,k)
    local permit = nil
    return function()
        local kpm = permit and permit()
        if not kpm then
            local pm = chooseit()
            if not pm then
                return nil
            end
            permit = perm(pm)
            kpm = permit()
        end
        return kpm
    end
end


--iterates over all tpl in t^k
ktuples = function(t,k,nshift)
    nshift = nshift or 0
    local tmax=#t
    local pos = 0    
    if k ~= 1 then
        local nlit = ktuples(t,k-1,nshift+1)
        pos=pos+1
        return function()
            local tpl = nlit()
            if (not tpl) and pos < tmax then
                pos = pos + 1
                nlit=ktuples(t,k-1,nshift+1)
                tpl = nlit()
            elseif (not tpl) and pos == tmax then
                return nil
            end
            tpl[nshift+1] = t[pos]
            return tpl
        end
    else
        return function()
            pos = pos + 1
            return ((pos <= tmax and {[nshift+1]=t[pos]}) or nil)
        end
    end
end

--iterates over all tpl in {1,...,indmax[1]} x ... x {1,...,indmax[k]}
--old impl. for reference, use ktuplesInd
ktuplesIndRec = function(indmax,k,nshift,rev)
    nshift = nshift or 0
    local imax=(type(indmax)=="table" and indmax[nshift+1]) or indmax
    k = k or #indmax
    local i = 0    
    if k > 1 then
        local nlit = ktuplesInd(indmax,k-1,nshift+1,rev)
        i=i+1
        if rev then
            return function()
                local tpl = nlit()
                if (not tpl) and i < imax then
                    i = i + 1
                    nlit=ktuplesInd(indmax,k-1,nshift+1,rev)
                    tpl = nlit()
                elseif (not tpl) and i == imax then
                    return nil
                end
                tpl[#tpl+1] = i
                return tpl
            end
        end
        return function()
            local tpl = nlit()
            if (not tpl) and i < imax then
                i = i + 1
                nlit=ktuplesInd(indmax,k-1,nshift+1,rev)
                tpl = nlit()
            elseif (not tpl) and i == imax then
                return nil
            end
            tpl[nshift+1] = i
            return tpl
        end
    else
        if rev then
            return function()
                i = i + 1
                return ((i <= imax and {i}) or nil)
            end
        end
        return function()
            i = i + 1
            return ((i <= imax and {[nshift+1]=i}) or nil)
        end
    end
end

--iterates over all tpl in {1,...,indmax[1]} x ... x {1,...,indmax[k]}
--zero: over {0,...,indmax[1]-1} x ... x {0,...,indmax[k]-1} instead
--rev: iterates in lexical order of reversed tuples
--(neither produces reversed tuples nor iterates in reversed order)
--i.e.: {1,1} {2,1} {3,1} instead of {1,1} {1,2} {2,1}
--if indmax={3,2} and zero is false/nil
ktuplesInd = function(indmax,zero,rev,nodcopy)
    if #indmax == 0 then
        return function() end
    end
    local t = {}
    local dcopy = (nodcopy and (function(t) return t end)) or dcopy
    indmax = dcopy(indmax)
    local init = (zero and 0) or 1
    local st,ed = (rev and #indmax) or 1, (rev and 1) or #indmax
    local by=(rev and 1) or -1
    for j=st,ed+by,-by do
        t[j] = init
        indmax[j] = indmax[j]-1+init
    end
    t[ed] = init-1
    indmax[ed] = indmax[ed]-1+init
    return function()
        for j=ed,st,by do
            t[j] = t[j] + 1
            if t[j] <= indmax[j] then--no further carry
                return dcopy(t)
            end
            t[j] = 1
        end
    end
end


fillWith = function(s,w,n)
    local t = {s}
    for i=1,n-#s do
        t[#t+1] = w
    end
    return table.concat(t)
end


isint = function(n)
  return type(n) == "number" and n == math.floor(n)
end

--table.maxn is removed but still useful for "sparse" arrays
if not table.maxn then
    table.maxn = function(t)
        local max=0
        for i,v in pairs(t) do
            if isint(i) and i > max then
                max = i
            end
        end
        return max
    end
end

--fills all values from 1 to table.maxn(t) with values according to 
--t[table.maxn(t)]
desparsify = function(t)
    local n = table.maxn(t)
    if (n<1) then
        return
    end

    local fill = (type(t[n]) == "string" and "") or 
            (type(t[n])=="number" and 0) or false
    for i=1,n do
        t[i] = t[i] or fill
    end
end

nilifeq = function(v,forbidden)
    return (v~=forbidden and v) or nil
end
