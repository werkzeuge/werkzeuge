rawequal = rawequal or function(x,y) return x==y end

  
local csv=require("csv")
local ods = require("loadodsfile")
dofile("options.lua")


local CSVTab,csvread,csvwrite = csv.CSVTab,csv.csvread,csv.csvwrite
local tonumberorna,NA = csv.tonumberorna,csv.NA
prnr=forceprnr or prnr or(pr[2] and pr[2].ordner and pr[2].ordner ~= "" and 2) or 1
pr=prconf[prnr]

pr.seatcodecol = pr.seatcodecol or "AA"
pr.seatvaluecol = pr.seatvaluecol or "AB"
pr.emptySheets = pr.emptySheets or 3

lpeg = require("lpeg")
local ersatz = {
  ["Ä"] = "A",
  ["Ö"] = "O",
  ["Ü"] = "U",
  ["Ç"] = "C",
  ["ä"] = "a",
  ["ö"] = "o",
  ["ü"] = "u",
  ["ß"] = "s",
  ["á"] = "a",
  ["á"] = "a",
  ["å"] = "a",
  ["å"] = "a",
  ["ç"] = "c",
  ["ç"] = "c",
  ["é"] = "e",
  ["é"] = "e",
  ["ğ"] = "g",
  ["ğ"] = "g",
  ["ı"] = "i",
  ["ł"] = "l",
  ["ó"] = "o",
  ["ó"] = "o",
  ["ş"] = "s",
  ["ş"] = "s",
}
local uml = lpeg.P("ä")
for u,_ in pairs(ersatz) do
  uml = uml + u
end
uml = lpeg.Cs((uml / ersatz + 1)^0)
--sortReplace kann in options.lua belegt sein
local sr = sortReplace or {}
sortReplace = function(s)
  return sr[s] or uml:match(s)
end

local colToTab = function(sp,cnr)
  local col,ret = ods.tableValues(sp,cnr,nil,cnr),{}
  for i,t in ipairs(col) do
    ret[i] = t[1].value
  end
  return ret
end


--Eingabe ist eine Liste aus Hörsaalcodes
--Ausgabe ist eine Liste von Sitzplatzverteilungen der passenden HS
lhuse = function(lhnames)
  if not lhnames then return nil end
  lhnames = kvswap(lhnames)
  local ret = {}
  local seatcodecolnr = ods.getRange(pr.seatcodecol)[1]
  local seatvaluecolnr = ods.getRange(pr.seatvaluecol)[1]
  sp = ods.getTable(
    ods.load(pr.path..'/'..pr.seatscheme):loadContent())
  seatcodes = colToTab(sp,seatcodecolnr)
  seatvalues = colToTab(sp,seatvaluecolnr)
  local curlh = {}
  for i=2,#seatcodes do
    if seatcodes[i] ~= "" then--neuer Hörsaal
      if lhnames[seatcodes[i]] then--HS ist angefordert
        curlh = {name=seatcodes[i], rows = {}}
        ret[lhnames[curlh.name]] = curlh
      else
        curlh = nil
      end
    end
    if curlh and tonumber(seatvalues[i]) then
      table.insert(curlh.rows,tonumber(seatvalues[i]))
    end
  end
  for _,lh in pairs(ret) do
    --rev in oo.lua dreht Array um
    lh.rows = rev(lh.rows)
  end
  return ret
end

if not pr.lhs then
  print("Fehler keine Hörsaalgruppen angegeben (Variable lhs)")
end

local lhs,lhGroups,lhc = {},{},0
for i,gr in ipairs(pr.lhs) do
  if type(gr) == "string" then
    gr = {gr}
  end
  lhGroups[i] = {}
  for _,h in ipairs(gr) do
    lhc=lhc+1
    lhs[lhc] = h
    table.insert(lhGroups[i],lhc)
  end
end
lhs = lhuse(lhs)
for _,g in pairs(lhGroups) do
  for i=1,#g do
    g[i] = lhs[g[i]]
  end
end

local sortBy=pr.sortBy or "Nachname"
if type(sortBy) == "string" then
  local sortKey = sortBy
  sortBy = function(r) return r[sortKey] end
end
local breakBy=pr.breakBy or "Nachname"
if type(breakBy) == "string" then
  local breakKey = breakBy
  breakBy = function(r)
    return sortReplace(r[breakKey]) or r[breakKey]
  end
end
local breakWhere = pr.breakWhere
if type(breakWhere) == "string" then
  breakWhere = {breakWhere}
end
if not breakWhere then
  breakWhere = {}
end

--pr.reserved wird direkt aus options.lua genutzt
teiln=csvread(studierendeGesPath,CSVsep,CSVstrdelRead)
teiln:transformColNonNa("MatrikelNr",tostring)
local sortname = "sortname"..prnr
if not teiln[sortname] then
  teiln:addCol("sortname","")
else
  teiln:colrename(sortname,"sortname")
end
if not teiln.Bemerkung then
  teiln:addCol("Bemerkung","")
end

local prcol = "PR"..prnr.."angemeldet"
local prFilter
if pr.inclKeinUES then
  prFilter = function(r,i)
    if not r[prcol] then return false end
    if not r.UESaktuell and r.UESalt==NA then
      teiln[{i,"Bemerkung"}] = "Zulassung prüfen!"
    end
    return true
  end
else
  prFilter = function(r)
    return (r.UESaktuell or r.UESalt ~=NA) and r[prcol]
  end
end


teiln = teiln:selectrows(prFilter)
teiln = teiln:colsub({"MatrikelNr","Nachname","Vorname","Bemerkung","CMS","sortname"})
teiln:addCol("Platz","")

idlist=csvread(pr.path.."/PR"..prnr.."_MNrID.csv")
if idlist then
  idlist:transformColNonNa("MatrikelNr",tostring)
  local nteiln = teiln:nrows()
  local nidlist = idlist:nrows()
  teiln = teiln:mergedWith(idlist,"MatrikelNr")
  if teiln:nrows() ~= nteiln or nteiln ~= nidlist then
    error("Teilnehmende passen nicht zu gespeicherter ID-Liste. "..
    'Bitte Datei "'..pr.path.."/PR"..prnr.."_MNrID.csv"..'" löschen.')
  end
else
  --für unsere Zwecke zufällig genug:
  math.randomseed(tonumber(tostring(os.time()):reverse():sub(1,6)))
  teiln:addCol("ID",0)
  local rt,rtk = {},{}
  for i=1,teiln:nrows()+pr.emptySheets do
    local r = math.random()
    --doppelte Werte vermeiden
    while rtk[r] do
      r = math.random()
    end
    rt[i] = r
    rtk[r] = i
  end
  table.sort(rt)
  for i=1,teiln:nrows() do
    teiln[{i,"ID"}] = rtk[rt[i]]
  end
  csvwrite(teiln:colsub({"ID","MatrikelNr"}),
    pr.path.."/PR"..prnr.."_MNrID.csv",CSVsep,CSVstrdelWrite)
end
local usedIds,emptySheetIds = kvswap(teiln.ID),{}
for i=1,teiln:nrows()+pr.emptySheets do
  if not usedIds[i] then
    table.insert(emptySheetIds,i)
  end
end

-- Spaltenordnung mit ID am Anfang (wieder) herstellen
teiln = teiln:colsub({"ID","MatrikelNr","Nachname","Vorname","Platz","Bemerkung","CMS","sortname"})

local seatstr = function(lhname,row,seat)
  return lhname .. " " .. ((row<10 and 0)or"") .. row .. "/" .. 
      ((seat<10 and 0)or"") .. seat
end
local resseats = {}

if pr.reserved then
  reserved = teiln:selectrows(function(r,i)
    local res = pr.reserved[r.MatrikelNr]
    if not res then return false end
    teiln[{i,"Platz"}] = seatstr(res.lhname,res.row,res.seat)
    resseats[seatstr(res.lhname,res.row,res.seat)] = true
    return true
  end)
  -- reservierte vorerst für die Zuteilung entfernen
  teiln=teiln:selectrows(function(r) return r.Platz == "" end)
end

-- teilnGr ist eine Liste von csv-Tabellen, 1 für jede Hörsaalgruppe
teilnGr = {}
if #lhGroups > 1 then
  for i=0,#lhGroups-1 do
    table.insert(teilnGr,teiln:selectrows(
        function(r)
          return (not breakWhere[i] or breakBy(r)>=breakWhere[i]) and
            (not breakWhere[i+1] or breakBy(r)<breakWhere[i+1])
        end
    ))
  end
else
  teilnGr = {teiln}
end

--gibt eine Liste aus Sitznamen des Hörsaal lh zurück
lhgToSeatList = function(lhg)
  local sl = {}
  for _,lh in ipairs(lhg) do
    local name = lh.name
    for i,r in ipairs(lh.rows) do
      for j=1,r do
        local s = seatstr(name,i,j)
        if not resseats[s] then
          table.insert(sl,s)
        end
      end
    end
  end
  return sl
end

--Einer Gruppe von Stud. (d.h. die in einer Hörsaalgr. schreiben)
--werden die Sitzplätze zugewiesen
assignSeats = function(tg,sl)
  local sortCol,rowi = {},{}
  for i=1,tg:nrows() do
    rowi[i] = i
    sortCol[i] = sortBy(tg:row(i,{names=true}))
  end
  local sortf = (pr.sortDec and
      function(i,j)return (sortReplace(sortCol[j]) or sortCol[j])
                <(sortReplace(sortCol[i]) or sortCol[i])
      end) or
      function(i,j)return (sortReplace(sortCol[i]) or sortCol[i])
                <(sortReplace(sortCol[j]) or sortCol[j])
      end
  table.sort(rowi,sortf)
  for i,is in ipairs(rowi) do
    tg[{is,"Platz"}] = sl[i] or NA
  end
end

teiln = teiln:cloneHead()
for i,tg in ipairs(teilnGr) do
  local sl = lhgToSeatList(lhGroups[i])
  assignSeats(tg,sl)
  teiln = teiln:concat(tg)
end
if reserved then
  teiln = teiln:concat(reserved)
end

if pr.finalSort then
  teiln = teiln:sorted(pr.finalSort,false,sortReplace)
end

noseat = teiln:selectrows(function(r)return r.Platz==NA end)
if noseat:nrows() > 0 then
  print("Folgende Personen haben keinen Sitzplatz:\n")
  print(noseat)
else
  local teilfile = pr.path.."/teilnehmendePr"..prnr..".csv"
  for _,id in pairs(emptySheetIds) do
    teiln:addRow({id,"","","","","","",""})
  end
  print("Jeder hat einen Platz, Datei "..teilfile.." wird erzeugt.")
  csvwrite(teiln,teilfile,';','"')
end
