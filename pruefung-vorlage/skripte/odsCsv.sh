cd `dirname $0`/..
for file in Agnes; do #Agnes MoodleKomplett
  soffice --headless --convert-to csv:"Text - txt - csv (StarCalc)":"59,34,22,1" "${file}.ods"
  #soffice returns while still writing with another process - thus we wait:
  #inotifywait -q -e close "$file.csv"
  sleep 5
  iconv -f iso-8859-15 -t utf8 "${file}.csv" | sponge "${file}.csv"
done
cd $OLDPWD
 
