--Nutzung make ZIEL ARG=argument (z.B. Prüfungsnummer)
package.path=package.path..";".."./skripte/?.lua"
ziel=""

--nicht anpassen
local targets,otgt,desc,toolsensure = {},{},{},nil
setmetatable(targets,{
    --Schlüssel von targets werden sortiert in otgt abgelegt 
    __newindex = function(self,k,v)
        rawset(targets,k,v)
        table.insert(otgt,k)
    end
})
local depends=function(...) 
    for _,t in ipairs({...}) do
        targets[t]()
    end
end
--Ende des "nicht anpassen"-Bereichs

targets.gesamtZuPr = function(nr)
    forceprnr = tonumber(nr)
    dofile('skripte/gesamtZuPr.lua')
end
desc.gesamtZuPr = "Teilnehmendenliste für die Prüfung Nr. ARG erstellen"

targets.help = function()
    print("Syntax: make ZIEL ARG=Argument (z.B. Prüfungsnummer)")
    print("Ziele:")
    fill15 = function(s)
        return s .. ("               "):sub(1,20-#s)
    end
    --wir listen die Ziele in der Reihenfolge des Erscheinens (otgt):
    for _,z in ipairs(otgt) do
        print(fill15(z),desc[z] or "","\n")
    end
end
desc.help = "Ziele auflisten"


local toolsminversion = 1
local toolscreateversion = 2

--nicht anfassen, allgemeine Funktion dieses Makefiles
local toolslua = io.open("tools.lua", "rb")
if toolslua then
    toolslua:close()
else
    platform = "unknown"
    --existiert das Windows-Nul-Device?
    local nul = io.open("nul","r")
    local devnull = io.open("/dev/null","r")
    if devnull then
        platform = "unix"
        devnull:close()
    end
    if nul then
        platform = "windows"
        nul:close()
    end
    toolslua = io.open("tools.lua","w")
    
    toolslua:write([==[
--An das eigene System anpassen und
--NICHT ins svn/git einchecken!
--Diese Datei wird nach den Zielen eingelesen, d.h. z.B. targets.vlseite
--ließe sich überschreiben
]==])
    toolslua:write('toolsversion='..toolscreateversion..'\n')
    toolslua:write('platform="'..platform..'"\n')
    if platform == windows then
        toolslua:write('rmcmd="del /p "\n')
    else
        toolslua:write('rmcmd="rm -vf "\n')
    end
    
    toolslua:write([==[
pdflatexbin="pdflatex"
lualatexbin="lualatex"
texextraoptions="-synctex=1"
openpdfafterrun=true

openpdfar = function(jobname)
    if openpdfafterrun then
        os.execute(openpdf .." " .. jobname .. ".pdf")
    end
end

runpdflatex = function(params,jobname)
    if platform == "unix" then
        params = "'" .. params .. "'"
    end
    if jobname then
        params = "-jobname " .. jobname .. " " .. params
    end
    local ex = pdflatexbin .. " " .. texextraoptions .. " " .. params 
    os.execute(ex)
    
    if jobname then
        local rerun = false
        print(jobname..".log")
        logf = io.open(jobname..".log", "r")
        for line in logf:lines() do
            if line:find("Package rerunfilecheck Warning") or
                line:find("Rerun to get cross%-references right") then
                rerun = true
                break
            end
        end
        logf:close()
        if rerun then
            os.execute(ex)
        end
        openpdfar(jobname)
    end
end
                      
runlualatex = function(params,jobname)
    if platform == "unix" then
        params = "'" .. params .. "'"
    end
    if jobname then
        params = "-jobname " .. jobname .. " " .. params
    end
    local ex = lualatexbin .. " " .. texextraoptions .. " " .. params 
    os.execute(ex)
    
    if jobname then
        local rerun = false
        print(jobname..".log")
        logf = io.open(jobname..".log", "r")
        for line in logf:lines() do
            if line:find("Package rerunfilecheck Warning") or
                line:find("Rerun to get cross%-references right") then
                rerun = true
                break
            end
        end
        logf:close()
        if rerun then
            os.execute(ex)
        end
        openpdfar(jobname)
    end
end
]==])

    toolslua:close()
end
dofile("tools.lua")

--sichert zu, dass var definiert ist und definiert es sonst mit val
--raw kann z.B. für Funktionen genutzt werden
toolsensure = function(var,val,raw)
    if not _ENV[var] then
        _ENV[var] = raw or val
        val = (not raw and type(val) == "string" and "[["..val .."]]")
                or val
        toolslua = io.open("tools.lua", "a")
        toolslua:write(var.."="..val.."\n")
        toolslua:close()
    end
end

if platform == "windows" then
    toolsensure("opentex",'start ""')
    toolsensure("openpdf",'start ""')
else
    toolsensure("opentex",'xdg-open')
    toolsensure("openpdf",'xdg-open')
end

if (toolsversion or 0) < toolsminversion then
    print("tools.lua veraltet, bitte ggf. sichern und dann löschen.")
    return
end

local targetlua = io.open("target.lua", "rb")
if targetlua then
    targetlua:close()
    dofile("target.lua")
end

ziel=arg[1] or ziel
marg=arg[2] or ziel

zielfkt = targets[ziel] or targets.help
zielfkt(marg)
-- TODO: bessere Argumentübergabe vom Makefile, damit nicht alles
-- immer ARG heißen muss und auch mehrere Argument möglich sind
-- ARGS="bl=1, etc=2" wäre eine Idee, ist aber zu unhandlich für dem
-- Standartfall
-- 	

-- kate: default-dictionary de_DE_frami; syntax Lua
-- kate: end-of-line unix; 
-- kate: indent-width 4;
-- kate: keep-extra-spaces false;
-- kate: remove-trailing-spaces none; replace-tabs true;
-- kate: show-tabs true;
-- kate: tab-indents true;
