--Syntax mit make: make <target>
--nur make listet targets auf


ziel="klausur"

vl="klausurplanung"
--ersetzen durch z.B.
-- vl="ethi"
jahr="JJJJ"
monat="MM"
tag="TT"
--prnr=1 oder 2
prnr=1
-- wspfad="ws20/einftheo"
dateiprefix="klausur"
aufgaben=dateiprefix .. jahr.."-" ..monat.."-"..tag ..".tex"
ergebnisse=vl.."-ergebnisse-"..jahr.."-"..monat.."-"..tag..".txt"
deckbl=vl.."-deckblaetter-"..jahr.."-"..monat.."-"..tag
blattprefix="klausur"..jahr.."-" ..monat.."-"..tag
--exercisesheets-Paket wird von gitlab geladen
exsheetsver="0.15.1"

--nicht anpassen
local targets,otgt,desc,toolsensure = {},{},{},nil
setmetatable(targets,{
    --Schlüssel von targets werden sortiert in otgt abgelegt 
    __newindex = function(self,k,v)
        rawset(targets,k,v)
        table.insert(otgt,k)
    end
})
local depends=function(...) 
    for _,t in ipairs({...}) do
        targets[t]()
    end
end
--Ende des "nicht anpassen"-Bereichs

targets.deckblatt = function()
    local defs = "\\input " ..
        deckbl .. ".tex"
    runlualatex(defs,deckbl)
end

targets.deckbltest = function()
    local defs = "\\def\\deckbltest{} \\input " ..
        deckbl .. ".tex"
    runlualatex(defs,deckbl)
end


--für A5-HK zum Hochladen, make web 01 erstellt  einftheo-blatt01.pdf
targets.klausur = function()
    depends("exsheets")
    local defs = "\\def\\bepunkt{false}\\def\\loes{false} \\input " ..
        aufgaben
    runlualatex(defs,blattprefix)
end

targets.loesung = function()
    depends("exsheets")
    local defs = "\\def\\loes{true} \\input " ..
        aufgaben
    runlualatex(defs,blattprefix.."-loesung")
end
desc.loesung = "Komplette schriftliche Lösung inkl. Bepunktung " ..
    "(sofern vorhanden - Umgebung gradingguides) und Lösung des "..
    "MC-Tests. Nicht für Studierende!"


targets.a4loesung = function()
    depends("exsheets")
    local defs = "\\def\\blatt{"..blnum..
        "} \\def\\loes{true}\\def\\avier{}"..
        "\\def\\bepunkt{true}\\def\\mitquiz{} \\input " ..
        aufgaben 
    runlualatex(defs,blattprefix.."-a4loesung")
end
desc.a4loesung = "Wie Lösung nur auf A4 (einspaltig, hochkant)"

targets.beamerloesung = function()
    depends("exsheets")
    local defs = "\\def\\blatt{"..blnum..
        "} \\long\\def\\ifbeamer#1#2{#1}\\def\\loes{oral}"..
        "\\def\\bepunkt{false}\\def\\mitquiz{} \\input " ..
        aufgaben 
    runlualatex(defs,blattprefix.."-loesungsfolien")
end
desc.beamerloesung = "Lösung zum vorstellen in Übung per "..
    "Videokonferenz oder Beamer, nur mündliche Lösungen, inkl. "..
    "MC-Test"


targets.jahraktuell = function()
    depends("exsheets")
    local pat = dateiprefix:gsub("[%^%$%(%)%%%.%[%]%*%+%-%?]","%%%0")
    local lfs=lfs or require("lfs")
    for ex in lfs.dir("exercises") do
        local num = ex:match("([0-9]+).*.tex")
        if num then
            local iofile = io.open("exercises/"..ex,"r+")
            local line = iofile:read()
            local content = ""
            while line do
                local tm = line:match(pat)
                if tm then
                    --Kommentierter Code ändert inplace
                    --das wäre effizienter, aber riskanter
--                     local bs = -#line-1
                    line = line:gsub(pat.."[0-9]+".."%.tex",
                            aufgaben)
--                     iofile:seek("cur",bs)
--                     iofile:write(line)
                end
                content = content .. line .. "\n"
                line = iofile:read()
            end
            --iofile:seek("set") würde uns auch zum Start bringen,
            --allerdings würde eine kürzere Ersetzung Reste lassen
            iofile:close()
            iofile = io.open("exercises/"..ex,"w")
            iofile:write(content)
            iofile:close()
        end
    end
end
desc.jahraktuell = "Aktualisiert verweise auf die Hauptdatei in "..
    "den Aufgabendateien, z.B. für Emacs."

targets.help = function()
    print("Ziele:")
    fill15 = function(s)
        return s .. ("               "):sub(1,20-#s)
    end
    --wir listen die Ziele in der Reihenfolge des Erscheinens (otgt):
    for _,z in ipairs(otgt) do
        print(fill15(z),desc[z] or "","\n")
    end
end
desc.help = "Ziele auflisten"

targets.exshclean = function()
    os.execute(rmcmd .. "exercisesheets*	exsh_lexercise.lua")
end
desc.exshclean = "Lösche exercisesheets"

targets.clean = function()
    os.execute(rmcmd .. "*.log *.out *.aux *~ *.bcf *.run.xml *.auxlock *.backup *.snm *.toc *.bbl *.blg *.vrb pool.tex")
end
desc.clean = "Lösche Hilfsdateien"

targets.pdfclean = function()
    os.execute(rmcmd .. "*.pdf *.synctex.gz *.nav")
end
desc.pdfclean = "Lösche PDFs und Navigationsdateien"

targets.mrproper = function()
    targets.clean()
    targets.pdfclean()
    targets.exshclean()
    os.execute(rmcmd .. "standardseite")
end
desc.mrproper = "clean+pdfclean+ standardseite löschen"

targets.exsheets = function()
    local extex = io.open("exercisesheets"..exsheetsver..".pdf", "rb")
    if extex then--existiert akt. Anleitung? Dann mache nichts!
        extex:close()
        return
    end
    --alte Anleitung löschen
    os.execute(rmcmd .. "exercisesheets*.pdf")
    local expre="curl https://gitlab.com/texedutools/exercisesheets/-/raw/"
    --spezifizierte Version herunterladen; Windows 10 enthält seit 2019 auch curl
    os.execute(expre..exsheetsver.."/exercisesheets.sty?inline=false -o exercisesheets.sty")
    os.execute(expre..exsheetsver.."/exercisesheets.tex?inline=false -o exercisesheets.tex")
    os.execute(expre..exsheetsver.."/exsh_lexercise.lua?inline=false -o exsh_lexercise.lua")
    --aktuelle Anleitung mit Versionsnummer erstellen
    runlualatex("exercisesheets.tex","exercisesheets"..exsheetsver)
end
desc.exsheets = "exercisesheets von gitlab beziehen"

local toolsminversion = 1
local toolscreateversion = 2

--nicht anfassen, allgemeine Funktion dieses Makefiles
local toolslua = io.open("tools.lua", "rb")
if toolslua then
    toolslua:close()
else
    platform = "unknown"
    --existiert das Windows-Nul-Device?
    local nul = io.open("nul","r")
    local devnull = io.open("/dev/null","r")
    if devnull then
        platform = "unix"
        devnull:close()
    end
    if nul then
        platform = "windows"
        nul:close()
    end
    toolslua = io.open("tools.lua","w")
    
    toolslua:write([==[
--An das eigene System anpassen und
--NICHT ins svn/git einchecken!
--Diese Datei wird nach den Zielen eingelesen, d.h. z.B. targets.vlseite
--ließe sich überschreiben
]==])
    toolslua:write('toolsversion='..toolscreateversion..'\n')
    toolslua:write('platform="'..platform..'"\n')
    if platform == windows then
        toolslua:write('rmcmd="del /p "\n')
    else
        toolslua:write('rmcmd="rm -vf "\n')
    end
    
    toolslua:write([==[
pdflatexbin="pdflatex"
lualatexbin="lualatex"
texextraoptions="-synctex=1"
openpdfafterrun=true

openpdfar = function(jobname)
    if openpdfafterrun then
        os.execute(openpdf .." " .. jobname .. ".pdf")
    end
end

runpdflatex = function(params,jobname)
    if platform == "unix" then
        params = "'" .. params .. "'"
    end
    if jobname then
        params = "-jobname " .. jobname .. " " .. params
    end
    local ex = pdflatexbin .. " " .. texextraoptions .. " " .. params 
    os.execute(ex)
    
    if jobname then
        local rerun = false
        print(jobname..".log")
        logf = io.open(jobname..".log", "r")
        for line in logf:lines() do
            if line:find("Package rerunfilecheck Warning") or
                line:find("Rerun to get cross%-references right") then
                rerun = true
                break
            end
        end
        logf:close()
        if rerun then
            os.execute(ex)
        end
        openpdfar(jobname)
    end
end
                      
runlualatex = function(params,jobname)
    if platform == "unix" then
        params = "'" .. params .. "'"
    end
    if jobname then
        params = "-jobname " .. jobname .. " " .. params
    end
    local ex = lualatexbin .. " " .. texextraoptions .. " " .. params 
    os.execute(ex)
    
    if jobname then
        local rerun = false
        print(jobname..".log")
        logf = io.open(jobname..".log", "r")
        for line in logf:lines() do
            if line:find("Package rerunfilecheck Warning") or
                line:find("Rerun to get cross%-references right") then
                rerun = true
                break
            end
        end
        logf:close()
        if rerun then
            os.execute(ex)
        end
        openpdfar(jobname)
    end
end
]==])

    toolslua:close()
end
dofile("tools.lua")

--sichert zu, dass var definiert ist und definiert es sonst mit val
--raw kann z.B. für Funktionen genutzt werden
toolsensure = function(var,val,raw)
    if not _ENV[var] then
        _ENV[var] = raw or val
        val = (not raw and type(val) == "string" and "[["..val .."]]")
                or val
        toolslua = io.open("tools.lua", "a")
        toolslua:write(var.."="..val.."\n")
        toolslua:close()
    end
end

if platform == "windows" then
    toolsensure("opentex",'start ""')
    toolsensure("openpdf",'start ""')
else
    toolsensure("opentex",'xdg-open')
    toolsensure("openpdf",'xdg-open')
end

if (toolsversion or 0) < toolsminversion then
    print("tools.lua veraltet, bitte ggf. sichern und dann löschen.")
    return
end

local targetlua = io.open("target.lua", "rb")
if targetlua then
    targetlua:close()
    dofile("target.lua")
end

ziel=arg[1] or ziel
blaetter=arg[2] or ziel

zielfkt = targets[ziel] or targets.help
zielfkt(blaetter)
-- TODO: bessere Argumentübergabe vom Makefile, damit nicht alles
-- immer BL heißen muss und auch mehrere Argument möglich sind
-- ARGS="bl=1, etc=2" wäre eine Idee, ist aber zu unhandlich für dem
-- Standartfall
-- 	

-- kate: default-dictionary de_DE_frami; syntax Lua
-- kate: end-of-line unix; 
-- kate: indent-width 4;
-- kate: keep-extra-spaces false;
-- kate: remove-trailing-spaces none; replace-tabs true;
-- kate: show-tabs true;
-- kate: tab-indents true;
