CSVsep=";"
--es werden immer auch Werte ohne String-Del. gelesen:
CSVstrdelRead='"'
--meist ist kein StrDel bei Schreiben nötig, da unsere Daten
--idR kein ; enthalten =>
CSVstrdelWrite=''

--ersetze Strings bei Sortierungen(z.B. für Umlaute und Sonderzeichen)
--idR nicht mehr nötig, nur nutzen, wenn die Reihenfolge falsch ist
sortReplace = {
  --  ["Original"] = Ersatz,
  -- z.B.:
  -- ["Çem"] = "Cem"
}

--Einstellungen für die Prüfungen
prconf={
  [1]={
--     path=nil,
    --Pfad zu den (meisten Prüfungsdaten)
    path="klausur1",
    --CSV aus der Prüferfunktion:
    --Spalten u.a. "sortname","mtknr"
    agnes=nil,
    --möglicherweise verschiedene Liste für den Upload
    --dort werden die Daten eingetragen
    --die Quelle muss das Suffix -export tragen
    -- z.B. 
    --agnesexp="prf_xxxx_xxxxx_xx.csv",
    agnesexp=nil,
    --manuelle Einträge in ähnlichem Format
    manuell="pr1manuell.csv",
    --Namen von Personen korrigieren oder ergänzen (CSV):
    namenkor=nil,
    --"sortname" bevorzugen (z.B. aus manuellen Einträgen)
    prefersortname=true,
    --Siztplätze in graphischer Darstellung
    seatscheme="sitzplan.ods",
    -- Spalten der Sitzplandatei (idR nicht ändern):
    -- seatcodecol="AA",
    -- seatvaluecol="AB",
  
    -- Anzahl leerer Blätter und zusätzlicher IDs (z.B.
    --für übersehene Übungsscheine und sonstige Fehler)
    -- emptySheets=3,
    emptySheets=3,
    --welche Hörsäle nutzen (siehe Sitzplan):
    --Hörsaalgruppen: innerhalb einer Gruppe werden die
    --Plätze einfach nach dem Sortierkriterium vergeben,
    --zwischen den Gruppen wird durch das Trennkriterium getrennt:
    --Standard ist: 0'115 ist eine Gruppe (aus 2 Teilhörsälen)
    --und 0'110 ist die andere (Ziffern bez. auf lh)
--     lhs={{"G1U","G2O"},{"KHS"}},
    --alternativ nur eine Gruppe, z.B.
    --     lh={{"G1U","G2O"}},
    lhs={{"KHS"},{"S07"},{"G1U"},{"G2O"}},
    --Sortierkriterium:
    --entweder einen Datenfeld:
    sortBy="Vorname",
    --oder eine Funktion von einer Zeile r:
    --sortBy=function(r) return r.MatrikelNr:reverse() end,
    --Sortierkriterium absteigend?
    sortDec=false,
    --Trennkriterium zwischen Hörsaalgruppen:
    --Optionen analog zu sortBy
    breakBy="Nachname",
    --Trennpunkte (Anzahl=Anzahl Hsgrp. - 1)
--     breakWhere={"N"},
    breakWhere={"H","L","Schm"},
    --Plätze für Studierende mit bes. Bedürfnissen reservieren
    --XXXXXX ist Mnr.
    --reserved={["XXXXXX"]={lhname="KHS",row=1,seat=1},
    --   ["YYYYYY"]={lhname="G1U",row=1,seat=1}}
    -- es dürfen auch in der Sitzplandatei nicht gelistete Räume
    -- für Reservierungen genutzt werden
    --(z.B. "EXT", "am Lehrstuhl" o.ä.):
    --reserved={["XXXXXX"]={lhname="am Lehrstuhl",row=1,seat=1}}
    -- wenn nichts reserviert:
    -- reserved = nil,
    reserved = nil,
    --Studierende ohne ÜS mit aufnehmen (wird in Bemerkung eingetr.)?
    inclKeinUES=false,
    finalSort="Name",
    --ods-Datei mit den Ergebnissen
    results="klausur-ergebnisse-JJJJ-MM-TT.ods",
    --soll die Upload-Datei mit ID und Note auch Punkte enthalten?
    uploadPoints=true,
  },
  [2]={
--     path=nil,
    path=nil,
    agnes=nil,
    agnesexp=nil,
    emptySheets=3,
    manuell="pr2manuell.csv",
    namenkor=nil,
    lhs={{"G1U"},{"G2O"},{"S10"}},
--     sortBy="Nachname",
    breakBy="Vorname",
    breakWhere={"F","O"},
    sortBy=function(r) return r.Nachname:sub(2) end,
    seatscheme="sitzplan.ods",
    reserved = {
      ["XXXXXX"]={lhname="EXT",row=1,seat=1},
    },
    inclKeinUES=false,
    finalSort="Platz",
    results="klausur-ergebnisse-MMMM-DD-TT.ods",
    uploadPoints=true,
  },
}
--Nummer der Prüfung manuell setzen (sonst als Argument für make)
--nur relevant für gesamtZuPr.lua und notenEintrVeroeff.lua
prnr=nil

studierendeGesPath="StudierendeGesamtBeispiel.csv"
--die folgende Datei ist die Datei mit den Daten aus Agnes, die aus
--der Anmeldung zu Anfang des Semesters generiert werden
--diese wird mit der odsCSV.sh konvertiert
agnesLehrePath="../Agnes.csv"
--MoodleExport zum Ende des Semesters
--als CSV mit Trenner ";" und *mit* Feedback exportieren
moodleGesPath="../MoodleKomplett.csv"
moodleGesamtstatus="Gesamtstatus (Punkte)"
moodleSchriftlich="schriftlich gesamt (Punkte)"
moodleMndlMC="Bestandene MC-Tests (Punkte)"
--folgende auf nil setzen, wenn nicht vorhanden
moodleCMS=nil--war: "CMS-Name (Rückmeldung)"
moodleMNR="Matrikelnummer"
--merge-Variable: "CMS" oder "MatrikelNr"
moodleAgnesMergeBy="MatrikelNr"
--Welchen Wert enthält die Spalte moodleGesamtstatus für best. ÜS?
moodleUESVal="bestanden"
uesAltPath="../../../uebersicht/ue.csv"
