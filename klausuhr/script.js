var started = false;
var full = 15;
var fullOrig = 120*60;
var startDate = 0;
var elapsed = 0;
var remaining = 0;
var finalFiveMin = false;
var finallNinetyNine = false;
var neverFinalFive = false;
var noPause = false;

var minute = "<span class='minsec'>'</span>";
var colon = '<span class="minsec">:</span>';

var fullscreen = function(){};
if(document.documentElement.requestFullscreen)
  fullscreen = function(){document.documentElement.requestFullscreen();}
else if(document.documentElement.mozRequestFullscreen)
  fullscreen = function(){document.documentElement.mozRequestFullscreen();}
else if(document.documentElement.webkitRequestFullscreen)
  fullscreen = function(){document.documentElement.webkitRequestFullscreen();}
else if(document.documentElement.msRequestFullscreen)
  fullscreen = function(){document.documentElement.msRequestFullscreen();};

var exitFullscreen = function(){};
if(document.exitFullscreen)
  exitFullscreen = function(){document.exitFullscreen();}
else if(document.mozCancelFullScreen)
  exitFullscreen = function(){document.mozCancelFullScreen();}
else if(document.webkitExitFullscreen)
  exitFullscreen = function(){document.webkitExitFullscreen();}
else if(document.msExitFullscreen)
  exitFullscreen = function(){document.msExitFullscreen();};

function init(){
  fullOrig = document.getElementById("fullmin").value * 60;
  full = fullOrig;
  remaining = full;
  document.getElementById("setup").style.visibility = "hidden";
  document.getElementById("wc").style.visibility = "hidden";
  drawTime();
  document.getElementById('set').blur();
  if(!document.getElementById('autoFullscreen').checked){
    fullscreen = function(){};
    exitFullscreen = fullscreen;
  }
  if(!document.getElementById('finalFive').checked){
    neverFinalFive = true;
  }
  if(document.getElementById('noPause').checked){
    noPause = true;
  }
}

function toggleClock(){
  if(started){
    if(noPause)
      return;
    full = remaining;
    started = false;
    exitFullscreen();
  } else {
    startDate = Math.floor(new Date().getTime()/1000);
    started = true;
    fullscreen();
  }
}
function toggleWC() {
  document.getElementById("wc").style.visibility = 
    document.getElementById("wc").style.visibility == "hidden" ?
    "visible" : "hidden";
}
function stepClock(dirKey){
  if(started){
    return;
  }
  var step = dirKey=="PageUp"?60:-60;
  full += step;
  remaining += step;
  drawTime();
}

window.onload=function(){
  document.getElementById("set").onclick=init;
  document.getElementById("time").ondblclick=toggleClock;
  document.getElementById("time").onclick=toggleWC;
};

document.addEventListener('keydown', function(event){
  if (event.key == " "){
    toggleClock();
  } else if ((event.key == "PageUp" || event.key == "PageDown")) {
    stepClock(event.key);
  } else if (event.key != "F5" && event.key != "F11") {
    toggleWC();
  }
});

function drawTime(){
  if (remaining <= 0) {
    started = false;
    document.body.style.backgroundColor = "white";
    document.getElementById("time").style.fontSize = "25vw";
    document.getElementById("time").style.color = "black";
    document.getElementById("time").innerHTML = "Abgabe";
    return;
  }

  if (remaining < 6000 && !finallNinetyNine){
    document.getElementById("time").style.fontSize = "50vw";
    finallNinetyNine = true;
  }

  if (remaining < 300 && !neverFinalFive && !finalFiveMin){
    finalFiveMin = true;
    document.body.style.backgroundColor = "red";
    document.getElementById("time").style.fontSize = "40vw";
    document.getElementById("secondsmini").style.visibility = "hidden";
  }

  if (finalFiveMin){
    document.getElementById("time").innerHTML = 
      (Math.floor(remaining / 60) + colon)+
      ((remaining % 60)<10?"0":"")+
      (remaining % 60);
  } else {
    document.getElementById("time").innerHTML = 
      Math.floor(remaining / 60) + minute;
    document.getElementById("secondsmini").innerHTML = remaining % 60;
  }
}

setInterval(function(){
  if(!started){
    return;
  }
  var now = Math.floor(new Date().getTime()/1000);
  elapsed = now - startDate;
  remaining = full - elapsed;

  drawTime();

}, 1000); 
