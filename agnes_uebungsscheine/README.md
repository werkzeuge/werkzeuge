# Übungsscheine in Agnes eintragen

Das Skript `agnes_uebungsteilnahme.py` ermöglicht das (semi-)automatische Eintragen von Übungsscheinen in Agnes.
Dies erfolgt, indem Studis, die in der Übung zugelassen/angemeldet sind,
auf den Status teilgenommen gesetzt werden.


## Voraussetzungen

### Programmabhängigkeiten

Das Skript basiert auf dem Selenium-Framework.
Hierzu muss die Pythonschnittstelle für Selenium installiert sein (getest für Version 4.7).
Dies kann bspw. via pip mit dem Befehl

    pip install [--user] selenium==4.7

erfolgen. Um Selenium zu nutzen, wird ein Webdriver benötigt,
also ein spezieller Browser, der von Selenium vergesteuert wird.
Dazu wird empfohlen, auch den webdriver-manager via

    pip install [--user] webdriver-manager==3.8

zu installieren.

Alternativ kann auch der Webdriver manuell heruntergeladen werden.
Das Skript nutzt den [Gecko-Driver](https://github.com/mozilla/geckodriver/releases),
welcher auf Firefox basiert.
Dieser muss ausführbar im selben Ordner wie das Skript liegen (sofern der Webdriver-Manager nicht genutzt wird).

### Skript-Argumente

Beachten Sie hier auch die Hilfe `agnes_uebungsteilnahme.py -h`.
Das Skript benötigt die Agnes-ID der Veranstaltung,
für die der Status gesetzt werden soll.
Diese ist in der URL der Detailansichtsseite als `publishid` zu finden.
Der Agnes-Nutzername, der dem Skript übergeben wird,
muss für diese Veranstaltung in den zugeordneten Personen stehen.
Zum Schluss muss dem Skript noch ein Dateipfad zu einer text-basierten Datei
gegeben werden.
In dieser befinden sich die Matrikelnummern.
Nicht-nummerische Zeichen (und Leerzeilen) werden ignoriert.
Nur eine Matrikelnummer pro Zeile.
Am Ende gibt das Skript die Matrikelnummern aus,
die nicht verarbeitet werden konnten.
