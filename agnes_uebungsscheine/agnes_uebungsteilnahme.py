#!/usr/bin/env python3

import argparse
import logging
import re
import sys
from typing import Set

from selenium.webdriver.support.wait import WebDriverWait

log = logging.Logger(__name__)
logging.getLogger().setLevel(logging.DEBUG)

try:
    from selenium import webdriver
    from selenium.webdriver.firefox.service import Service as FirefoxService
    from selenium.webdriver.common.keys import Keys
    from selenium.common.exceptions import NoSuchElementException, ElementNotInteractableException, \
        WebDriverException
    from selenium.webdriver.common.by import By
except ModuleNotFoundError:
    print('Selenium ist nicht installiert.')
    print('kann lokal mittels "pip install --user selenium==4.7" installiert werden')
    sys.exit(-1)


try:
    from webdriver_manager.firefox import GeckoDriverManager

    def get_driver() -> webdriver.Firefox:
        return webdriver.Firefox(service=FirefoxService(GeckoDriverManager().install()))

except ModuleNotFoundError:
    log.warning('Webdriver-Manager nicht installiert; kann mittels\n'
                '$ pip[3] install [--user] webdriver-manager==3.8\n'
                'nachinstalliert werden. Ohne dieses muss der GeckoDriver vorher heruntergeladen werden '
                'und im Arbeitsverzeichnis liegen. (https://github.com/mozilla/geckodriver/releases)')

    def get_driver() -> webdriver.Firefox:
        service = FirefoxService(executable_path='./geckodriver')
        return webdriver.Firefox(service=service)


class LastGroupFound(Exception):
    """wird genutzt um zu signalisieren, dass es keine weiteren Agnes-Gruppen mehr für die Veranstaltung gibt"""
    pass


class AgnesBrowser:

    def pause(self, amount=5):
        """
        Gib der Seite Zeit zu laden.

        Teilweise ist das Skript so schnell, dass die Seite noch nicht bereit ist,
        den nächsten Befehl entgegenzunehmen.
        Daher muss an einigen Stellen (vor allem nach Seitenaufrufen) diese Methode eingefügt werden.
        Standardmäßig wartet sie bis zu 5 Sekunden (meist weniger),
        kann aber auch länger warten (siehe parameter amount).

        """
        self.driver.implicitly_wait(amount)

    def __init__(self, course_id, username, interactive=False, any_group=False, update=False):
        """
        Erstellt einen mit Skript-ansprechbaren Firefox-Browser.
        Dazu muss Firefox bereits installiert sein.
        Zusätzlich wird der GekoDriver benötigt.
        Der Driver kann von https://github.com/mozilla/geckodriver/releases heruntergeladen werden
        oder mittel dem Webdriver-Manager-Modul installiert werden

        Parameter
        =========

        course_id:   Die Agnes-ID des Kurses, für den Zulassungen vergeben werden sollen.
                     Diese kann bspw. auf der Detailseite der Veranstaltung in der URL als pubilishid gefunden werden
        username:    Der Agnes-Nutzername, mit dem die Zulassungen eingetragen werden sollen.
        interactive: Falls False, werden die Zulassungen so weit es geht automatisch eingetragen.
                     Falls True, werden die Zulassungen für eine Gruppe gesetzt und dann pausiert das Skript.
                     Der Nutzer kann so die Anpassungen prüfen, auf "Speichern" drücken, um das Skript fortzusetzen.
        any_group:   Falls False, werden Studis in der Übung auf "teilgenommen" gesetzt, in der sie "zugelassen" sind.
                     Andernfalls werden sie in irgendeiner (aber nur in einer) Gruppe auf "teilgenommen" gesetzt.
        update:      Falls gesetzt, werden Studis, die bereits auf "teilgenommen" gesetzt sind, ignoriert.
        """
        self.interactive = interactive
        self.any_group = any_group
        self.update = update
        self.asi = None
        self.username = username
        self.course_id = course_id
        try:
            int(course_id)
        except ValueError:
            raise ValueError('Kurs-ID muss einem ganzzahligen Wert entsprechen')

        self.driver = get_driver()

        self.driver.get('https://agnes.hu-berlin.de')
        self.pause()
        self.driver.find_element(By.CLASS_NAME, 'hu_loginlinks').click()
        user_form = self.driver.find_element(By.ID, 'username')

        user_form.clear()
        user_form.send_keys(username)

        pw_form = self.driver.find_element(By.ID, 'password')
        pw_form.clear()

        print('Jetzt das Passwort in den nächsten 2 min im Browser eingeben und einloggen. ')
        try:
            wait = WebDriverWait(self.driver, 120)
            wait.until(lambda driver: 'agnes.hu-berlin.de' in driver.current_url)
            # warte bis die URL wieder "agnes.hu-berlin.de" enthält. Login sollte dann abgeschlossen sein
            self.pause()
        except KeyboardInterrupt:
            self.driver.quit()
            self.driver = None
            sys.exit(0)

        for link in self.driver.find_elements(By.TAG_NAME, 'a'):
            # versuche, die ASI herauszufinden
            if 'asi=' in link.get_property('href'):
                self.asi = link.get_property('href').split('asi=')[1]
                self.asi = self.asi.split('&amp')[0]
                break
        else:
            print('Das Skript konnte die ASI (eine Art Sitzungs-ID für agnes) nicht ermitteln.')
            print('Bitte gehen Sie selbst zu der Teilnahme- und Zulassungsliste und suchen Sie dort die ASI in der URL')
            self.asi = self.get_input('ASI: ')
            if not self.asi:
                try:
                    self.asi = re.search('asi=([^&])', self.driver.current_url).group(1)
                except AttributeError:
                    print('konnte asi nicht aus aktueller URL extrahieren, breche ab')
                    sys.exit(-1)

    def set_participation(self, matnr_set: Set[str]) -> (int, Set[str]):
        """
        Iteriert über alle Gruppen der Veranstaltung und setze die Zulassung.
        Gibt die Anzahl der durchlaufenen Gruppen und eine Menge von Matrikelnummern zurück,
        die nicht bearbeitet/gefunden worden sind.
        """
        group_number = 1
        for group_number in self._get_group_side():
            self.set_participation_in_group(group_number, matnr_set)
        return group_number - 1, matnr_set

    def set_participation_in_group(self, group_number: int, matnr_set: Set[str]):
        """
        Setze die Teilnahme/Zulassung in der Gruppe ${group_number}.
        Die Teilnahme wird nur dann gesetzt, wenn der entsprechende Studi in der Gruppe auch angemeldet ist.
        Somit werden Doppelzulassungen vermieden.
        Gib die Matrikelnummern zurück,
        """
        log.debug(f'Setze in Gruppe {group_number} Matrikelnummern in {matnr_set} auf teilgenommen')
        matnr_pattern = re.compile('[1-9][0-9]{5}')

        for table in self.driver.find_elements(By.TAG_NAME, 'table'):
            # todo es gibt zwei Tabellen auf der Seite. Die erste ist zwar recht kurz,
            # todo besser wäre natürlich, irgendwie nur die "richtige" Tabelle durchzulaufen
            for tr in table.find_elements(By.TAG_NAME, 'tr'):
                try:
                    matnr = matnr_pattern.match(tr.text).group(0)
                except AttributeError:
                    # es konnte keine Matrikelnummer extrahiert werden
                    continue
                if matnr in matnr_set:
                    # todo was wenn das sub versehentlich eine Matrikelnummer erzeugt?
                    accepted = None
                    participation = None
                    for radio in tr.find_elements(By.TAG_NAME, 'input'):
                        if radio.get_attribute('value') == 'TE':
                            participation = radio
                        if radio.get_attribute('value') == 'ZU':
                            accepted = radio
                    if participation is None:
                        # todo dieser Fall sollte eigentlich nicht eintreten
                        continue
                    if self.update and participation.get_attribute('checked'):
                        matnr_set.discard(matnr)
                    elif self.any_group or (accepted and accepted.get_property('checked')):
                        participation.click()
                        matnr_set.discard(matnr)
        if self.interactive:
            self.get_input('Bitte Eingabe prüfen und bei Korrektheit auf "Speichern" drücken. '
                           'Danach hier Enter drücken. (Strg+C für Abbruch)')
        else:
            try:
                self.driver.find_element(By.NAME, 'submit').click()
            except (NoSuchElementException, ElementNotInteractableException):
                self.get_input('Der Speicher-Button kann nicht gedrückt werden. '
                               'Bitte manuell betätigen und durch Enter-Eingabe fortfahren')

    def add_missing(self, missing: Set[str]) -> Set[str]:
        with_error = set()
        group_url = 'https://agnes.hu-berlin.de/lupo/rds?state=wwrite&write=info&par=old&add.' \
                    f'{self.course_id}=1&show=lehrender&asi={self.asi}'
        self.driver.get(group_url)
        for matnr in missing:
            self.pause()
            try:
                self.driver.find_element(By.CSS_SELECTOR, 'a.FakeButton').click()
                self.pause()
            except (NoSuchElementException, ElementNotInteractableException):
                self.get_input('Der "Neuer Datensatz"-Button kann nicht gedrückt werden, breche ab')
                break
            self.driver.find_element(By.NAME, '0.2').send_keys(matnr)
            self.driver.find_element(By.NAME, '0.6').click()
            for radio in self.driver.find_elements(By.TAG_NAME, 'option'):
                if radio.get_attribute('value') == 'TE':
                    radio.click()
                    break
            self.driver.find_element(By.NAME, "post").click()
            self.pause()
            if self.driver.find_elements(By.CLASS_NAME, 'box_error'):
                with_error.add(matnr)
                self.driver.get(group_url)
        return with_error

    def __del__(self):
        if self.driver is not None:
            self.driver.quit()

    def get_input(self, prompt: str):
        try:
            return input(prompt)
        except KeyboardInterrupt:
            self.driver.quit()
            sys.exit(-1)

    def _get_group_side(self):
        """
        Der eigentliche Iterator, der nach und nach von einer Gruppenseite zur nächsten geht.
        """
        group_number = 0
        while True:
            group_number += 1
            self.driver.get('https://agnes.hu-berlin.de/lupo/rds?state=wwrite&write=info&par=old&add.'
                            f'{self.course_id}={group_number}&show=lehrender&asi={self.asi}')
            self.pause()
            if self.driver.find_elements(By.TAG_NAME, 'table'):
                yield group_number
            else:
                return


def parse_matnr_file(filename: str) -> Set[str]:
    """
    Hilfsfunktion, die aus der angegebenen Datei eine Menge aus Matrikelnummern macht.
    Die Datei sollte pro Zeile genau eine Matrikelnummer (mit 6 Ziffern ohne führende Null) haben.
    Nicht-numerische Zeichen und fehlerhafte Zeilen werden ignoriert.
    """
    matnr_set = set()
    try:
        with open(filename, 'r') as fd:
            for lino, line in enumerate(fd, 1):
                matnr = re.sub('[^0-9]', '', line)
                if len(matnr) == 6 and matnr[0] != '0':
                    matnr_set.add(matnr)
                elif matnr:
                    log.warning(f'{matnr} in Zeile {lino} wird ignoriert (Falsches Format)')
    except FileNotFoundError:
        pass
    log.debug(f'Folgende Matrikelnummern gefunden: {matnr_set}')
    return matnr_set


class ArgParseWithHelp(argparse.ArgumentParser):

    def error(self, message):
        sys.stderr.write(f'error: {message}\n')
        self.print_help()
        sys.exit(2)


def main():
    arg_parser = ArgParseWithHelp(description="""
    Ein Programm, um Übungsscheine in Agnes einzutragen.
    Hierzu wird eine Datei mit Matrikelnummern eingelesen
    und dann mittels Selenium (einem Tool/Framework,
    das einen Browser per Skript steuern kann) in Agnes
    der Status dieser Matrikelnummern auf TE (teilgenommen/zugelassen) gesetzt.
    """)
    arg_parser.add_argument('-c', '--kursid', dest='course_id', required=True,
                            help='Die ID der Agnes-Veranstaltung, u.a. zu finden in der URL der Veranstaltungsseite '
                                 '(im Vorlesungsverzeichnis) als publishid')
    arg_parser.add_argument('-i', '--interactive', action='store_true',
                            help='Falls gesetzt wird der Nutzer nach jeder Gruppe gebeten, die Eingabe zu überprüfen '
                                 'und selbst zu speichern.')
    arg_parser.add_argument('-g', '--any-group', action='store_true',
                            help='Falls gesetzt werden Studis in der erstbesten Gruppe auf "teilgenommen" gesetzt, '
                                 'sonst nur in der Gruppe, in der sie zugelassen sind.')
    arg_parser.add_argument('--update', action='store_true',
                            help='Falls gesetzt werden Studis, die bereits auf "teilgenommen" gesetzt sind, ignoriert.')
    arg_parser.add_argument('-a', '--add-missing', action='store_true',
                            help='Falls gesetzt werden verbleibende Matrikelnummern als neuer Datensatz ergänzt.')
    arg_parser.add_argument('-u', '--nutzer', dest='username', required=True,
                            help='Der Agnes-Nutzername für die Anmeldung.')
    arg_parser.add_argument('Matrikelnummern',
                            help='Pfad zu einer Textdatei, die die Matrikelnummern der einzutragenden Studis enthält. '
                                 'Jede Zeile sollte nur eine Matrikelnummer '
                                 'mit genau 6 Ziffern und ohne führende 0 haben.')
    args = arg_parser.parse_args()

    browser = AgnesBrowser(args.course_id, args.username, interactive=args.interactive, update=args.update)
    matnr_set = parse_matnr_file(args.Matrikelnummern)

    max_group, remaining_matnr = browser.set_participation(matnr_set)
    if remaining_matnr:
        print(f'Folgende Matrikelnummern konnten in den ersten {max_group} nicht gefunden werden:\n{remaining_matnr}')
        if args.add_missing:
            print('Die verbleibenden Matrikelnummern werden nun in Gruppe 1 als "teilgenommen" hinzugefügt.')
            with_error = browser.add_missing(remaining_matnr)
            if with_error:
                print(f'Bei folgenden Matrikelnummern gab es einen Fehler beim Nachtragen:\n{with_error}')


if __name__ == '__main__':
    main()
