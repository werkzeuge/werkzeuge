# Digitale Klausureinsicht

Da es ggf. ungewünscht ist, dass Studis die bewerteten PDFs zum untereinander vergleichen haben, hier ein Vorschlag zur parallelisierten Vorbereitung / Ablauf einer digitalen Einsicht über BBB (Zoom-Alternative, von der HU gehostet).
**Disclaimer:** über Screenshots bzw. andere Umwege kommt man noch an die Dateien.

## Ablauf:
1. auf "bbb.hu-berlin.de" anmelden
2. "create a room" für jeden Studi mit generiertem Moderatorcode, benötigter Bestätigung zum Joinen und dass jeder das Meeting starten kann
3. Intern in einem Etherpad alle Links zu den Räumen mit Zuordnung und Moderator-codes speichern
4. auf der Hauptseite bei dem Raum auf die Optionen (Punkte) -> "Add a presentation" und entsprechende Klausur auswählen
5. den Studi den Link zum Raum incl. Moderator-Code (damit sie Präsentator haben) geben (Moodle Direktnachrichten und mit Zeitslots)
6. über ein öffentliches Etherpad sich die Studis mit Aufgabe melden lassen und dann entsprechender Korrektor dem Raum joinen (ggf. Notizen für interne Absprache hinter die Meldungen)
7. Nach der Einsicht Räume über die Webseite löschen

## mögliche Probleme:
- die PDFs sind nicht sofort sichtbar:
	BBB braucht einen Moment (bis zu 2 min) bis die Dateien verarbeitet sind; im Raum beim "Plus (unten links)->upload presentation" ist der aktuelle Stand ersichtlich
- die PDF hat weiße Seiten statt dem eigentlichen:
	vermutlich ist das ein Problem mit dem Format / der größe. Workaround: über Windows "print to file" wird es behoben und es kann als neue Datei hochgeladen werden, die funktioniert (entweder über Raumeinstellungen oder Plus wie oben, letzteres ist temporär und wird nicht im Raum gespeichert)
- ich möchte was ändern, habe aber keinen presenter:
	auf euer Icon in der rechten Seite klicken und "take presenter"
- der Ton des Voicechats funktioniert nicht
    BBB kann bei geringer Bandbreite Probleme verursachen. ggf. Zoom-Meeting als Backup planen

## Möglicher Text für das öffentliche Etherpad:
Wir werden euch ab angemeldeter Zeit einen Link zu einem BBB-Raum geben. Dieser ist nur mit dem dazugegeben Code betretbar und darüber gesichert.
1. geht auf den angegebenen Link
2. gebt den Moderator-Code ein und drückt "Enter" daneben
3. danach betretet den Raum ("Join")

In dem Raum ist eure Klausur hochgeladen. An der unteren Seite der Dokumente könnt ihr Seiten wechseln, vergrößern etc.
Schaut euch eure Klausur an.
Wenn ihr eine Frage zur Bewertung oder technische Probleme habt, geht in dieses Etherpad und meldet euch im Textfeld bei uns mit dem Name und der Aufgabennummer (wir haben die Links, Codes und IDs, behaltet diese geheim); wir werden dann zu euch in den BBB-Raum kommen (kann einige Minuten dauern, wenn wir anderen helfen).
Ihr könnt euch die Klausur auch länger als eure 20 Minuten anschauen.